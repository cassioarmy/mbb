Rails.application.routes.draw do
  
  root 'home#index' 

  scope(:path_names => { :new => "novo", :edit => "editar" }) do

    #location urls
    get "localizacao/cidades/estado/:state_id" => "location#cities_from_state"
    get "localizacao/estados" => "location#states"

    get "osid/novo" => "service_order_item_description#create" 
    get "osid/descricao" => "service_order_item_description#description_items", :as => "description_items"   

    resources :vehicles, :path => "veiculos", except: :destroy do
      collection do
        get "/:state_id/cities_from_state" => "vehicles#cities_from_state", :as => "cities_from_state", :format => :json
        get "/find" => "vehicles#find", :as => "find"
      end
    end

    resources :customers, :path => "clientes", except: :destroy do
      collection do
        get "/buscarVeiculos" => "customers#search_vehicle", :as => "search_vehicle"        
        get "/find" => "customers#find", :as => "find"
      end
    end

    resources :service_orders, :path => "os" do
      collection do
        get "/descricao" => "service_orders#description_items", :as => "description_items"
        get "/buscar" => "service_orders#find", :as => "buscar"
        get "/:id/emissao" => "service_orders#report", :as => "report"
        post "/enviar/email" => "service_orders#send_mail", :as => "send_mail"
      end
    end

    resources :users, :path => "usuarios", except: :destroy

    resource :company, :path => "empresa", :controller => :company, :only => [:edit, :show, :update, :create]

    devise_for :users, path: "", path_names: { sign_in: 'entrar', sign_out: 'sair', password: 'senha', confirmation: 'confirmacao', unlock: 'desbloqueio', registration: 'registro', sign_up: 'cadastro' }
  end

end
