#FROM ruby:2.6.3-alpine3.9 
FROM ruby:2.3.8-slim-jessie

RUN mkdir /mecanica

WORKDIR /mecanica

ENV RAILS_SERVE_STATIC_FILES=true
ENV RAILS_LOG_TO_STDOUT=true
ENV RAILS_ENV=development
ENV TZ=America/Sao_Paulo

ADD Gemfile ./Gemfile
ADD Gemfile.lock ./Gemfile.lock

ADD . /mecanica
RUN apt-get update -qq && apt-get install -y build-essential nodejs npm nodejs-legacy mysql-client libmysqlclient-dev bundler libfontconfig1 libxrender1 libxext6 && \
    bundle install
    #\
    #bundle exec rake assets:precompile


