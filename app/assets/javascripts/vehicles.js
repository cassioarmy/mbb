var VehiclesJS = new function() {

	this.initInputMasks = function() {
		$("#vehicle_license_plate").inputmask("***-****");   
        $("#vehicle_year_model").inputmask("****-****");   
        $("#license_plate").inputmask("***-****");
	};

	this.initCombos = function() {
		var stateIdCombo = $("#state_state_id");

		stateIdCombo.change(function(event) {
			VehiclesJS.carregarCidades($(this).val());
		});
	};

	this.carregarCidades = function(stateId) {

		$("#vehicle_city_id option").remove();

		var vehicleComboId = $("#vehicle_city_id");

		vehicleComboId.append("<option>Selecione...</option>");

		$.get( "/localizacao/cidades/estado/" + stateId + ".json", function(data) {
		  $.each(data, function(key, value) {

		  	var optionTag = "<option value='" + value.id + "'>" + value.name + "</option>";

		  	vehicleComboId.append(optionTag);
		  });
		});
	};

}