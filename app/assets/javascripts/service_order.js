var ServiceOrder = new function (){
	
	this.init = function (){
		//var dtPagamento = $('.input-group.date').datepicker({format:'dd/mm/yyyy',language: "pt-BR",autoclose: true,todayHighlight: true});
        

        //adiciona javascripts nos campos criados acima
        //componente de data
        
        $('.table-data-serv-prestado tbody tr').each(function(index){            
            obj = $('#service_order_service_order_item_attributes_'+index+'_soi_date')
            $(obj).datepicker({dateFormat: 'dd/mm/yy'});
            $(obj).next().on('click',function(){
                $(this).prev().datepicker('show','dateFormat','dd/mm/yy');
            });
        });

        $('#service_order_payment_date').datepicker({dateFormat: 'dd/mm/yy'});
        $('#payment-date').click(function(){
            $(this).prev().datepicker('show','dateFormat','dd/mm/yy');
        });


        //plugin
        //http://loopj.com/jquery-tokeninput/
        /*$(".descricao").tokenInput("/osid/descricao",{
            theme:"bootstrap",
            tokenLimit: 1,
            hintText: "Digite para buscar",
            noResultsText: "Sem resultados",
            searchingText: "Buscando...",
            searchDelay:200,
            propertyToSearch:"description",
            minChars:4
        }); */        

        $(".descricao").autocomplete({

          source:"/osid/descricao",          
          select: function( event, ui ) {
            event.preventDefault();

            $(this).next().next('input').val( ui.item.value );
            $(this).val( ui.item.label );

          }
        });

        //carrega descrições para os campos tokenInput
         /*$(".descricao").each(function(){
            if( $(this).attr("value") !== undefined && $(this).attr("value") != ''){
                idSearch ="#desc"+($(this).attr("value")); 
                desc = $("#desc"+($(this).attr("value")) ).attr("value");
                $(this).tokenInput("add", {id: $(this).val(), description: desc+"" });

                //solucao para salvar na edição
                $(this).val($(this).attr("value"))
            }
         });*/

         //tamanho dinamico autosugest
         //var w = $(".token-input-list-bootstrap").outerWidth();
         //$("div.token-input-dropdown-bootstrap").css("width",w+"px") 

         ServiceOrder.saveDescription();
         ServiceOrder.buttonStatus();
         ServiceOrdersCustomersJS.initCustomersSearchForm("#service_order_customer_id", "#customerNameArea", "#customerAddressArea", "#customerPhonesArea", "#service_order_vehicle_id");
         ServiceOrder.mask();

         //calcula o valor total dos serviços
         ServiceOrder.calcularTotalServicosPrestados();
         ServiceOrder.limpaFormatacaoDinheiro();
         ServiceOrder.validaClickPago();
         
	};

    this.initMailingAction = function() {
        $(".send-mail").click(function(event) {
            var soId = $(this).attr("so-id");
            $("#so_id").val(soId);
        });
    };

    this.mask = function(){
        $(".valor").maskMoney({thousands:'.',decimal:',',prefix:'',affixesStay:false});
        $(".valor").each(function(){
            $(this).maskMoney('mask');
            $(this).on("blur",function(){ServiceOrder.calcularTotalServicosPrestados()});
        });
    };


	this.buttonStatus = function(){
        $(".btnstatus").on("click",function (){            
            valor = $(this).next().val()
            if(valor == 'true' || valor == '1'){
                $(this).removeClass("btn-success")
                $(this).addClass("btn-danger")
                $(this).next().val('false')
                $(this).text("Não")
            }else{
                $(this).removeClass("btn-danger")
                $(this).addClass("btn-success")                
                $(this).next().val('true')
                $(this).text("Sim")
            }
        });
    };

    this.buttonStatusTodos = function(){
        $(".btnstatus").on("click",function (){            
            valor = $(this).next().val()
            if(valor == 'true' || valor == '1'){
                $(this).removeClass("btn-success")
                $(this).removeClass("btn-danger")
                $(this).addClass("btn-info")
                $(this).next().val('')
                $(this).text("Todos")
            }else if(valor == 'false' || valor == '0'){
                $(this).removeClass("btn-danger")
                $(this).removeClass("btn-info")
                $(this).addClass("btn-success")                
                $(this).next().val('true')
                $(this).text("Sim")
            }else{
                $(this).removeClass("btn-info")
                $(this).removeClass("btn-success")
                $(this).addClass("btn-danger")                
                $(this).next().val('false')
                $(this).text("Não")
            }
        });
    };

    this.saveDescription = function(){
        //quando mudar o valor verifica
        $("td .descricao").on("change",function (event){ 
            event.preventDefault()
            //verifica se é numero, se não for cadastra
            //a descrição se for numero da replace para
            //comparação com vazio após
            valor = $(this).val().replace( /\d/g , "");
            
            if(valor != ''){
                var objAux = $(this).nextAll('input');
                //mostra modal cadastro de serviço
                $("#loading_modal").modal({
                        backdrop:'static'
                });

                //cadastra o serviço
                var getNovo = $.get("/osid/novo.json",{description:$(this).val()},
                    function(){});

                getNovo.done(function(item){                    
                    //salvando o valor no proprio campo para no submit salvar                  
                    $(objAux).val(item.id);                
                    $("#loading_modal").modal("hide");

                });

                getNovo.fail(function(evt, jqXHR, ajaxSettings, thrownError ){                     
                    $("#loading_modal").modal("hide");
                    alert(ajaxSettings);
                });
            }

        })
    };

    this.adicionarServicoPrestado = function(){
        var tabela = $(".table-data-serv-prestado tbody tr")
        var total = tabela.length

        var data = '<div class="input-group date">'+
                        '<input class="form-control" id="service_order_service_order_item_attributes_'+total+'_soi_date" name="service_order[service_order_item_attributes]['+total+'][soi_date]" value="" type="text">'+
                        '<span class="input-group-addon"><i class="fa fa-calendar"></i></span>'+
                       '</div>';

        //var idDescricao = 'service_order_service_order_item_attributes_'+total+'_service_order_item_description_id';
        //var descricao = '<input class="form-control descricao" id="'+idDescricao+'" name="service_order[service_order_item_attributes]['+total+'][service_order_item_description_id]" value="" type="text">';
        var descricao = '<input class="form-control descricao" id="desc'+(total)+'" name="desc'+(total)+'" type="text" value="" autocomplete="off">';
        var descricaoHidden = '<input class="form-control ui-autocomplete-input" id="service_order_service_order_item_attributes_'+(total)+'_service_order_item_description_id" name="service_order[service_order_item_attributes]['+(total)+'][service_order_item_description_id]" style="display:none" type="text" value="" autocomplete="off">';

        var idTotalCampo = 'service_order_service_order_item_attributes_'+(total)+'_unit_price';
        var totalCampo   = '<input class="form-control valor" id="'+idTotalCampo+'" name="service_order[service_order_item_attributes]['+(total)+'][unit_price]" value="0.0" type="text">'

        var botaoExcluir = ' <button class="remover-servico-prestado btn btn-danger btn-sm center-block" id="removerItem" name="button" type="button" onclick="ServiceOrder.removerServicoPrestado(this)">'+
                              '<i class="fa fa-trash-o"></i>'+
                            '</button>';

        var linha   = "<tr>";
            linha  += '<td class="form-group has-feedback ">'+data+"</td>";
            linha  += '<td class="form-group has-feedback ">'+descricao+descricaoHidden+"</td>";
            linha  += '<td class="form-group has-feedback ">'+totalCampo+"</td>";
            linha  += '<td class="form-group has-feedback ">'+botaoExcluir+"</td>";
            linha  += "</tr>";

        //adiciona linha na tabela
        $(".table-data-serv-prestado tbody").append(linha);


        //adiciona javascripts nos campos criados acima
        //componente de data
        //$('.input-group.date').datepicker({format:'dd/mm/yyyy',language: "pt-BR",autoclose: true,todayHighlight: true});

        //adiciona javascripts nos campos criados acima
        //componente de data
        $('.table-data-serv-prestado tbody tr').each(function(index){            
            obj1 = $('#service_order_service_order_item_attributes_'+index+'_soi_date')
            $(obj1).datepicker({dateFormat: 'dd/mm/yy'});
            $(obj1).next().on('click',function(){
                $(this).prev().datepicker('show','dateFormat','dd/mm/yy');
            });
        });        

        //componente auto sugeste
        /*$("#"+idDescricao).tokenInput("/osid/descricao",{
            theme:"bootstrap",
            tokenLimit: 1,
            hintText: "Digite para buscar",
            noResultsText: "Sem resultados",
            searchingText: "Buscando...",
            searchDelay:200,
            propertyToSearch:"description",
            minChars:4
        }); */

        $(".descricao").autocomplete({

          source:"/osid/descricao",          
          select: function( event, ui ) {
            event.preventDefault();

            $(this).next().next('input').val( ui.item.value );
            $(this).val( ui.item.label );
            
          }
        });

        //tamanho dinamico autosugest
         var w = $(".token-input-list-bootstrap").outerWidth();
         $("div.token-input-dropdown-bootstrap").css("width",w+"px") 

        $("#"+idTotalCampo).maskMoney({thousands:'.',decimal:',',prefix:'R$ ',affixesStay:false});
        $("#"+idTotalCampo).on("blur",function(){ServiceOrder.calcularTotalServicosPrestados()});
        //chama para adicionar eventos para poder salvar a descrição
        //quando ela não existir na base de dados
        ServiceOrder.saveDescription();
    }

    this.calcularTotalServicosPrestados = function(){
        var valorTotal = 0.0;
        $(".valor").each(function(){
            //se for desconto nao calcula
            if($(this).attr("id") != 'service_order_discount'){
                valor = $(this).val().replace(".","").replace(",",".").replace( /R\$/g , "");
                valorTotal += parseFloat(valor);
            }
        });
        $(".total-geral").text( "R$ "+Common.formatarReais(valorTotal) );

    };

    this.limpaFormatacaoDinheiro = function(){
        $("form").on("submit",function(){
           $(".valor").each(function(){
            valor = $(this).val().replace(".","").replace(",",".").replace( /R\$/g , "");
            $(this).val(valor);
            }); 
        });
    };

    this.limpaFormularioBusca = function(){
        $("form input").each(function(){
            $(this).val("");
        });
        $("form select").each(function(){
            $(this).val("");
        });
        $(".btnstatus").each(function(){
            $(this).removeClass("btn-success")
            $(this).removeClass("btn-danger")
            $(this).addClass("btn-info")
            $(this).next().val('')
            $(this).text("Todos")
        });
    };

    this.removerOS = function(obj){
        //adiciona classe no tr
        $(obj).parent().parent().parent().parent().parent().addClass("danger");
        $("#removeros_modal").modal({
                backdrop:'static'
        });
    };

     this.removerServicoPrestado = function(obj){

        $("#removersp_modal").modal({

                backdrop:'static'

        });
        $(obj).parent().parent().addClass("danger");
    };



    this.removerPopup = function(){

        //removendo hidden para remover registro
        valor = $('.table-data-serv-prestado tr.danger').data("id");
        $('.table-data-serv-prestado tr.danger').remove();

        if(valor != ''){

            //obrigatorio passar o id e a permissão para destruir
            var hidden = '<input id="service_order_service_order_item_attributes_'+valor+'_service_order_item_description_id" name="service_order[service_order_item_attributes]['+valor+'][id]" type="hidden" value="'+valor+'">'+

            '<input id="service_order[service_order_item_attributes]['+valor+'_destroy" name="service_order[service_order_item_attributes]['+valor+'][_destroy]" type="hidden" value="true" >';

            $('.table-data-serv-prestado').parent().append(hidden); 
        }

        $("#removersp_modal").modal("hide");
    };

    this.validaClickPago = function(){
        $('.pagobtn').click(function(e){
            valor = $("#service_order_payment_type").val();            
            if(valor == 7 || valor == 9){
                //retornando valores ao estado padrao
                $(this).removeClass("btn-success");
                $(this).removeClass("btn-danger");
                $(this).addClass("btn-danger");
                $(this).text("Não");
                $("#service_order_paid").val("false");

                //exibindo mensagem                
                $("#pagoMSGError").modal({
                    backdrop:'static'
                });
            }
        });
    };
};