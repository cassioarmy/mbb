ServiceOrdersCustomersJS = new function() {

	var selectedCustomerIdField = null;
	var selectedCurrentCustomersList = null;

	this.initCustomersSearchForm = function(customerField, customerName, customerAddress, customerPhones, customerVehicles) {

		$("#abrirModalClientesButton").click(function(event) {
	    	event.preventDefault();
	    	ServiceOrdersCustomersJS.loadCustomersTable("", "", 1);
	    });

	    $("#buttonOk").click(function(event) {
	    	var selectedData = $.grep(selectedCurrentCustomersList.customers, function(elementOfArray, indexInArray) {
	    		if (elementOfArray.id == selectedCustomerIdField) {
	    			return elementOfArray;
	    		}
	    	});

	    	$(customerField).val(selectedData[0].id);
	    	if($(customerName).is("input")){
	    		$(customerName).val(selectedData[0].name);
	    	}else{
	    		$(customerName).text(selectedData[0].name);
	    	}
	    	$(customerAddress).text(selectedData[0].address);

	    	var customerPhonesVar = "";

	    	$.each(selectedData[0].phones, function(key, value) {
	    		customerPhonesVar += value.number + " Contato: " + value.contact + " / ";
	    	});

	    	$(customerPhones).text(customerPhonesVar);

    		$(customerVehicles + " option").remove();
    		customerVehiclesCombo = $(customerVehicles);
    		customerVehiclesCombo.append("<option>Selecione...</option>");

    		$.each(selectedData[0].customervehicle, function(key, value) {
    			formatada_placa="";
    			if(value.license_plate != ""){
    				formatada_placa = value.license_plate.substring(0,3)+"-"+value.license_plate.substring(3,7)
    			}
	    		customerVehiclesCombo.append("<option value='"+value.id+"'>"+formatada_placa+" - "+value.description+"</option>");
	    	});

	    	$('#dados_cliente').show('slow');

	    	ServiceOrdersCustomersJS.cleanCustomersTable();
	    });

	    $("#buttonCancel").click(function() {
	    	selectedCustomerIdField = null;
	    	ServiceOrdersCustomersJS.cleanCustomersTable();
	    });

	    $("#searchCustomersButton").click(function(event) {
	    	event.preventDefault();
	    	var paramToSearch = $("#searchCustomersField").val();
	    	ServiceOrdersCustomersJS.loadCustomersTable(paramToSearch, paramToSearch, 1);
	    });
	};

	this.cleanCustomersTable = function() {
		$("#customersTable tbody tr").removeClass("activetr");
		$("#customersTable tbody tr").remove();
		$("#customersTablePagination").hide();
		$("#customersTablePagination ul li").remove();
	};

	this.loadCustomersTable = function(nameParam, cpfCnpjParam, pageParam) {

		ServiceOrdersCustomersJS.cleanCustomersTable();

		var tableBody = $("#customersTable tbody");

		$.get("/clientes/find.json", {
				name: nameParam, 
				cpf_cnpj: cpfCnpjParam, 
				page: pageParam, 
				per_page_param: 10, 
				order_by_param: "name"
			}, function(data) {

				selectedCurrentCustomersList = data;

				$.each(data.customers, function(key, value) {
					var currentRow = $("<tr>");

					currentRow.append("<td>"+value.id+"</td>");
					currentRow.append("<td>"+value.name+"</td>");

					tableBody.append(currentRow);
				});

				if (data.pagination.total_pages > 1) {
					var customersTablePaginationUl = $("#customersTablePagination ul");

					var liPrevious = $("<li class='" + (data.pagination.current_page == 1 ? "disabled" : "") + "'>");
					liPrevious.append("<a href='#' page-number='"+(data.pagination.current_page > 1 ? (data.pagination.current_page - 1) : 1)+"'>&laquo;</a>");
					customersTablePaginationUl.append(liPrevious);

					for (i = 1 ; i <= data.pagination.total_pages ; i++) {
						var liPage = $("<li class='" + (data.pagination.current_page == i ? "active" : "") + "'>");
						liPage.append("<a href='#' page-number='"+i+"'>"+i+"</a>");
						customersTablePaginationUl.append(liPage);
					}

					var liNext = $("<li class='" + (data.pagination.current_page == data.pagination.total_pages ? "disabled" : "") + "'>");
					liNext.append("<a href='#' page-number='"+(data.pagination.current_page < data.pagination.total_pages ? (data.pagination.current_page + 1) : data.pagination.total_pages)+"'>&raquo;</a>");
					customersTablePaginationUl.append(liNext);

					$("#customersTablePagination").show();

					ServiceOrdersCustomersJS.initPagination();
					
				}

				ServiceOrdersCustomersJS.initTableRowClick();
		});

	};

	this.initTableRowClick = function() {
		$("#customersTable tbody tr").click(function() {
	        $("#customersTable tbody tr").removeClass("activetr");
	        if (!$(this).hasClass("header")) {
        		$(this).addClass("activetr");
	        	selectedCustomerIdField = $(this).find("td:first").text();
	        }
	    });
	};

	this.initPagination = function() {
		$("#customersTablePagination a").click(function(event) {
			event.preventDefault();
			var pageToGo = $(this).attr("page-number");
			ServiceOrdersCustomersJS.loadCustomersTable("", "", pageToGo);
		});
	};

}