$(document).ready(function () {

    $.rails.allowAction = function(link) {
      if (!link.attr('data-confirm')) {
        return true;
      }
      $.rails.showConfirmDialog(link);
      return false;
    };

    $.rails.confirmed = function(link) {
      link.removeAttr('data-confirm');
      return link.trigger('click.rails');
    };


    $.rails.showConfirmDialog = function(link) {
      var html, message;
      message = link.attr('data-confirm');
      // html = "<div class=\"modal\" id=\"confirmationDialog\">\n  <div class=\"modal-header\">\n    <a class=\"close\" data-dismiss=\"modal\">×</a>\n    <h3>Are you sure Mr. President?</h3>\n  </div>\n  <div class=\"modal-body\">\n    <p>" + message + "</p>\n  </div>\n  <div class=\"modal-footer\">\n    <a data-dismiss=\"modal\" class=\"btn\">Cancel</a>\n    <a data-dismiss=\"modal\" class=\"btn btn-primary confirm\">OK</a>\n  </div>\n</div>";
      $("#confirmationDialog").modal("show");
      return $('#confirmationDialog .confirm').on('click', function() {
        return $.rails.confirmed(link);
      });
    };


	$('select[data-option-dependent=true]').each(function (i) {
            var observer_dom_id = $(this).attr('id');
            var observed_dom_id = $(this).data('option-observed');
            var url_mask = $(this).data('option-url');
            var key_method = $(this).data('option-key-method');
            var value_method = $(this).data('option-value-method');
            var prompt = $(this).has('option[value=]').size() ? $(this).find('option[value=]') : $('<option value=\"\">').text('Select a specialization');
            var regexp = /:[0-9a-zA-Z_]+:/g;
            var observer = $('select#' + observer_dom_id);
            var observed = $('#' + observed_dom_id);

            if (!observer.val() && observed.size() > 1) {
                observer.attr('disabled', true);
            }
            observed.on('change', function () {
                observer.empty().append(prompt);
                if (observed.val()) {
                    url = url_mask.replace(regexp, observed.val());
                    var request = $.ajax({url: url, format: "json",beforeSend:beforeSend});

                    request.done( function (data) {
                            $.each(data, function (i, object) {
                                observer.append($('<option>').attr('value', object[key_method]).text(object[value_method]));
                                observer.attr('disabled', false);
                            });
                            
                            $('#loading_modal').modal('hide')
                    });
                }
            });
        });

    //retirar mascara para submeter form
    $.extend($.inputmask.defaults, {
     'autoUnmask': true
    });
    
    //verifica se é mobile
    //e mostra somente 3 colunas
    if(isMobile()){

        $("table.table-data td").each( function(index ){
            if(index > 1 && !($(this).hasClass("last")) ){
                $("table.table-data th:eq("+index+")").hide();
                $(this).hide();
                
            }
        });
    }
});


function beforeSend(xhr){
    xhr.setRequestHeader("Accept", "application/json");
    $("#loading_modal").modal({
        backdrop:'static'
    });
}


/**
* @function isMobile
* detecta se o useragent e um dispositivo mobile
*/
function isMobile()
{
    var userAgent = navigator.userAgent.toLowerCase();
    if( userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 )
        return true;
}



function formatarTelefone(classe){
    //formatação inicial
    $(""+classe).each(function(){
        valor = $(this).val().replace( /\D/g , "");
        if(valor.length <= 10)
            $(this).inputmask({mask:"(99)9999-9999[9]"});
        else if(valor.length > 10)
            $(this).inputmask("(99)99999-9999");

    });
    
   $(""+classe).on("keyup", function(){
      valor = $(this).val().replace( /\D/g , "");
      if(valor.length <= 10)
        $(this).inputmask({mask:"(99)9999-9999[9]"});
      else if(valor.length > 10)
        $(this).inputmask("(99)99999-9999");
    });  
}


Common = new function(){      

    this.initCombos = function(stateID, cityID) {
        var stateIdCombo = $(stateID);
        this.city_id = cityID;
        stateIdCombo.change(function(event) {
            Common.carregarCidades($(this).val());
        });
    };

    this.carregarCidades = function(stateId) {
        $("#loading_modal").modal({
            backdrop:'static'
        });
        $(Common.city_id+" option").remove();

        $(Common.city_id).append("<option>Selecione...</option>");

        $.get( "/localizacao/cidades/estado/" + stateId + ".json", function(data) {
          $.each(data, function(key, value) {

            var optionTag = "<option value='" + value.id + "'>" + value.name + "</option>";

            $(Common.city_id).append(optionTag);
          });
          $('#loading_modal').modal('hide')
        });
        
    };

    this.buttonStatus = function(hidden){
        $("#btn-status").on("click",function (){            
            if($(hidden).val() == '1'){
                $(this).removeClass("btn-success")
                $(this).addClass("btn-danger")
                $(hidden).val('0')
                $(this).text("Inativo")
            }else{
                $(this).removeClass("btn-danger")
                $(this).addClass("btn-success")                
                $(hidden).val('1')
                $(this).text("Ativo")
            }
        });
    }

    this.carregarTodosEstados = function(idCombo){
        $("#loading_modal").modal({
            backdrop:'static'
        });
        $.get( "/localizacao/estados.json", function(data) {
          var optionTag='<option value="">Selecione...</option>';
          $.each(data, function(key, value) {

            optionTag +=  "<option value='" + value.id + "'>" + value.name + "</option>";                        
            
          });
          $(idCombo).append(optionTag);
        });
        $('#loading_modal').modal('hide')
    };

    this.formatarReais= function(num){        

       x = 0;

       if(num<0) {
          num = Math.abs(num);
          x = 1;
       }
       if(isNaN(num)) num = "0";
          cents = Math.floor((num*100+0.5)%100);

       num = Math.floor((num*100+0.5)/100).toString();

       if(cents < 10) cents = "0" + cents;
          
          for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
             num = num.substring(0,num.length-(4*i+3))+'.'
                   +num.substring(num.length-(4*i+3));
            
        ret = num + ',' + cents;

        if (x == 1) ret = ' - ' + ret;return ret;
    };
}