// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery-1.11.0.min
//= require jquery_ujs
//= require turbolinks
//= require jquery.inputmask
//= require jquery.inputmask.extensions
//= require jquery.inputmask.numeric.extensions
//= require jquery.inputmask.date.extensions
//= require jquery-ui.min
//= require jquery.maskMoney.min
//= require bootstrap311/js/bootstrap.min
// require bootstrap-datepicker
// require jquery.tokeninput
//= require highcharts/highcharts
//= require highcharts/highcharts-more
//= require custom_rest_methods
//= require common
//= require customer
//= require vehicles
//= require company
//= require service_order
//= require service_order_item_description
//= require service_orders_customers
//= require ckeditor/config
//= require ckeditor/notification/plugin
//= require ckeditor/wordcount/plugin
