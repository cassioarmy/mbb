Customer = new function(){

	this.init = function(){

		$('#tabs').tab();
        
        //mascarando valor inicial
        valor = $("#customer_cpf_cnpj").val().replace( /\D/g , "");
        if(valor.length <= 11)
          $("#customer_cpf_cnpj").inputmask({mask:"999.999.999-99[9]"});
        else if(valor.length > 11)
          $("#customer_cpf_cnpj").inputmask("99.999.999/9999-99");

        $("#customer_cpf_cnpj").on("keyup", function(){
          valor = $(this).val().replace( /\D/g , "");
          if(valor.length <= 11)
            $(this).inputmask({mask:"999.999.999-99[9]"});
          else if(valor.length > 11)
            $(this).inputmask("99.999.999/9999-99");
        });

        Customer.buscarVeiculosButton();
        Customer.removerVeiculo();
        
        Customer.addPhone();
        Customer.removePhone();

        Customer.addSR();
        Customer.removeSR();

        formatarTelefone(".phoneNumbers");
        $("#customer_cpf_cnpj").focus();
        $("#customer_cep").inputmask("99999-999");        

        //registra evento cidades
        $("#state_id").change(function(){
        	Common.city_id = "#customer_city_id"
        	Common.carregarCidades($(this).val());
        });

        Customer.registraEventos();
	};

	this.buscarVeiculosButton = function(){
		$("#buscarVeiculos").on("click",function(){
			$("#loading_modal").modal({
		        backdrop:'static'
		    });

			var descricao = $("#description").val();
			var placa = $("#license_plate").val();
			var customerid = $("#customerid").val();
			$.get("/clientes/buscarVeiculos",{description:descricao,license_plate:placa,customerid:customerid}).done(function(data){
				Customer.mostrarVeiculos(data);
				Customer.addVeiculos();
			});

		});
	};

	this.removerVeiculo = function(){
		$(".removerVeiculo").on("click",function(){
			
			$("#remover_veiculo_modal").modal({
		        backdrop:'static'
		    });
			
			$(this).parent().parent().addClass("danger");
		});
	};

	this.removerVeiculoPopup = function(){
		var customer_vehicle_id = $(".table-data-add-veiculos tr.danger").data("id");
		
		if(customer_vehicle_id != '' ){
			var valor = $("#removerVeiculos").val()
			if(valor == ''){
				$("#removerVeiculos").val(customer_vehicle_id)
			}else{
				$("#removerVeiculos").val(valor+","+customer_vehicle_id)
			}
		}
		if($(".table-data-add-veiculos tr.danger input") != "undefined"){			
			$(".table-data-add-veiculos tr.danger input").attr("disabled","true");
		}
		$(".table-data-add-veiculos tr.danger").remove();
		
		$("#remover_veiculo_modal").modal("hide");
	};

	this.mostrarVeiculos = function(data){
		$("#tabela").html(data);
		
		$('.pagination a').attr('data-remote', 'true');

		$(".pagination a").each( function(index ){
			//antes de enviar mostrar popup
			$(this).bind("ajax:beforeSend",function(evt, data, status, xhr){			
				$("#loading_modal").modal({
		        	bindackdrop:'static'
		    	});
			});

			$(this).bind("ajax:success",function(evt, data, status, xhr){
				mostrarVeiculos(data)
			});
		});
		
		$("#loading_modal").modal("hide");
	};

	this.addVeiculos = function(){
		$(".addVeiculos").on("click",function(){	
			var tr = $(this).parent().parent();	
			var id = $(this).parent().data("id");
			var desc = $(tr).find("td").eq(0).text();
			var plate = $(tr).find("td").eq(1).text();
			var idx = $(".table-data-add-veiculos tbody tr").length
			//remove ultima coluna		
			var botaoRemover = '<button class="removerVeiculo btn btn-danger btn-xs center-block" id="removerVeiculo" name="button" type="button">'+
							                '<i class="fa fa-trash-o"></i>'+
							'</button>';
			var hidden = '<input type="hidden" name="customer[customer_vehicle_attributes]['+idx+'][vehicle_id]" value="'+id+'"/>';
							
			var linha = "<tr><td>"+desc+"</td><td>"+plate+"</td><td>"+hidden+botaoRemover+"</td></tr>"
			
			
			$(".table-data-add-veiculos tbody").prepend(linha);
			$(tr).remove();
			Customer.removerVeiculo();
		});
	};

	this.addPhone = function (){
		$(".addPhone").on("click",function(){
			var idx = $(".table-phones tbody tr").length
			var numero = '<input class="form-control phoneNumbers" id="customer_phones_attributes_'+idx+'_number" name="customer[phones_attributes]['+idx+'][number]" type="text">';
			var tipo = '<select class="form-control" id="customer_phones_attributes_'+idx+'_tp" name="customer[phones_attributes]['+idx+'][tp]"><option value="">Selecione...</option><option value="CELULAR">CELULAR</option>'+
						'<option  value="FIXO">FIXO</option></select>';
			var contato = '<input class="form-control" id="customer_phones_attributes_'+idx+'_contact" name="customer[phones_attributes]['+idx+'][contact]" type="text" maxlength="50">';			
			var operadora = '<select class="form-control" id="customer_phones_attributes_'+idx+'_provider" name="customer[phones_attributes]['+idx+'][provider]"><option value="">Selecione...</option><option value="VIVO">VIVO</option>'+
								'<option value="CLARO">CLARO</option>'+
								'<option value="OI">OI</option>'+
								'<option value="TIM">TIM</option>'+
								'<option value="OUTROS">OUTROS</option></select>';

			var botaoRemover = '<button class="removerPhone btn btn-danger center-block" id="removerPhone'+idx+'" name="button" type="button">'+
							    '            <i class="fa fa-trash-o"></i>'+
								'</button>';
			var linha = "<tr><td>"+numero+"</td><td>"+tipo+"</td><td>"+contato+"</td><td>"+operadora+"</td><td>"+botaoRemover+"</td>";

			$(".table-phones tbody").prepend(linha);

			Customer.removePhone();
        	//chama formatacao
        	formatarTelefone(".phoneNumbers");
		});
	};

	this.removePhone = function(){
		$(".removerPhone").on("click",function(){
			$(this).parent().parent().addClass("danger");
			$("#remover_phone_modal").modal({
		        backdrop:'static'
		    });
		});
	};
	
	this.removePhonePopup = function(){
		
		//removendo hidden para remover registro
		obj = $('.table-phones tr.danger td .removerPhone').next();
		valor = $(obj).val();

		$('.table-phones tr.danger td .removerPhone').next().remove();

		$('.table-phones tr.danger').remove();

		//obrigatorio passar o id e a permissão para destruir
		var hidden = '<input id="customer_phones_attributes_'+valor+'_id" name="customer[phones_attributes]['+valor+'][id]" type="hidden" value="'+valor+'">'+
		'<input id="customer_phones_attributes_'+valor+'_destroy" name="customer[phones_attributes]['+valor+'][_destroy]" type="hidden" value="true" >';
		
		$('.table-phones').parent().append(hidden);		

		$("#remover_phone_modal").modal("hide");
	};

	this.addSR = function(){
		$(".adicionarSR").on("click",function(){
			var idx = $(".table-state-registrations tbody tr").length
			var numero = '<input class="form-control" id="customer_state_registrations_attributes_'+idx+'_number" name="customer[state_registrations_attributes]['+idx+'][number]" type="text" maxlength="50">';
			
			var description = '<input class="form-control" id="customer_state_registrations_attributes_'+idx+'_description" name="customer[state_registrations_attributes]['+idx+'][description]" type="text" maxlength="250">';
			
			var estados = '<select class="form-control" id="state_id_'+idx+'" name="state_id'+idx+'" data-id_city="city_id_'+idx+'"></select>';
			
			var cidades = '<select class="form-control" id="city_id_'+idx+'" name="customer[state_registrations_attributes]['+idx+'][city_id]"></select>';
			
			var botaoRemover = '<button class="removerSR btn btn-danger center-block" id="removerSR'+idx+'" name="button" type="button">'+
							    '            <i class="fa fa-trash-o"></i>'+
								'</button>';
			
			var linha  = '<tr><td>'+numero+'</td><td>'+description+'</td><td>'+estados+'</td><td>'+cidades+'</td><td>'+botaoRemover+'</td></tr>';
			$(".table-state-registrations tbody").prepend(linha);
			
			//carregando estados			
			Common.carregarTodosEstados('#state_id_'+idx);

			//adicionando ações para quando clicar no combo
			//registra evento para carregar cidade
			$('#state_id_'+idx).change(function(){   	
	        	Common.city_id = "#"+($(this).data("id_city"));
        		Common.carregarCidades($(this).val());
	        });

			Customer.removeSR();
		});
	};

	this.removeSR= function(){
		$(".removerSR").on("click",function(){
			$(this).parent().parent().addClass("danger");
			$("#remover_sr_modal").modal({
		        backdrop:'static'
		    });
		});
	};


	this.removeSRPopup = function(){
		
		//removendo hidden para remover registro
		obj = $('.table-state-registrations tr.danger td .removerSR').next();
		valor = $(obj).val();

		$('.table-state-registrations tr.danger td .removerSR').next().remove();

		$('.table-state-registrations tr.danger').remove();

		//obrigatorio passar o id e a permissão para destruir
		var hidden = '<input id="customer_state_registrations_attributes_'+valor+'_id" name="customer[state_registrations_attributes]['+valor+'][id]" type="hidden" value="'+valor+'">'+
		'<input id="customer_state_registrations_attributes_'+valor+'_destroy" name="customer[state_registrations_attributes]['+valor+'][_destroy]" type="hidden" value="true" >';
		
		$('.table-state-registrations').parent().append(hidden);		

		$("#remover_sr_modal").modal("hide");
	};

	this.registraEventos = function(){
		
        //adição de funcionalidade ajax combos inscricao estadual
        //em cada linha da tabela
        $(".table-state-registrations tbody tr").each(function(){        	
        	var idState = $(this).find(".state").attr("id");
        	
			var idCity = $(this).find(".city").attr("id");
			
			//registra o dado para depois obter para fazer a busca
			$("#"+idState).data("id_city",idCity);

        	$("#"+idState).change(function(){   	
	        	Common.city_id = "#"+($(this).data("id_city"));
        		Common.carregarCidades($(this).val());
	        });
        });
	};
}