CompanyJS = new function() {

	this.init = function() {
		//mascarando valor inicial
        valor = $("#company_cnpj").val().replace( /\D/g , "");
        $("#company_cnpj").inputmask("99.999.999/9999-99");

        $("#customer_cpf_cnpj").on("keyup", function(){
          valor = $(this).val().replace( /\D/g , "");
          $(this).inputmask("99.999.999/9999-99");
        });

        formatarTelefone(".phone");
	};

}