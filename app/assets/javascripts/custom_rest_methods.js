function _ajax_request(url, data, callback, callback_error, type, method) {
    if (jQuery.isFunction(data)) {
        callback = data;
        data = {};
    }
    return jQuery.ajax({
        type: method,
        url: url,
        data: data,
        success: callback,
        error: callback_error,
        dataType: type
        });
}

jQuery.extend({
    put_: function(url, data, callback, callback_error, type) {
        return _ajax_request(url, data, callback, callback_error, type, 'PUT');
    },
    delete_: function(url, data, callback, callback_error, type) {
        return _ajax_request(url, data, callback, callback_error, type, 'DELETE');
    },
    post_: function(url, data, callback, callback_error, type) {
        return _ajax_request(url, data, callback, callback_error, type, 'POST');
    },
    get_: function(url, data, callback, callback_error, type) {
        return _ajax_request(url, data, callback, callback_error, type, 'GET');
    }
});