
if( typeof(CKEDITOR) !== "undefined" ){
    CKEDITOR.plugins.addExternal('wordcount', '/assets/ckeditor/wordcount/plugin');
    CKEDITOR.plugins.addExternal('notification', '/assets/ckeditor/notification/plugin');
    CKEDITOR.editorConfig = function (config) {
        // ... other configuration ...
        config.extraPlugins = 'wordcount,notification';
        config.toolbar_mini = [
            ["Bold",  "Italic",  "Underline",  "Strike",  "-",  "Subscript",  "Superscript"],
        ];
        config.toolbar = "mini";
        config.wordcount = {
            showParagraphs: false,
            showWordCount: true,
            showCharCount: true,
            countSpacesAsChars: true,
            countHTML: true,
            maxWordCount: -1,
            maxCharCount: 4000
        };
        config.enterMode =  CKEDITOR.ENTER_BR;
    }
}
  