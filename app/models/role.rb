class Role < ActiveRecord::Base
	has_and_belongs_to_many :users

	validates :name, presence: :true

	scope :all_but_admin, -> { where("name != 'Admin'") }
end
