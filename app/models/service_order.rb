class ServiceOrder < ActiveRecord::Base
  belongs_to :vehicle
  belongs_to :user
  belongs_to :customer, :foreign_key => 'customer_id'

  validates :customer_id, presence: :true  
  validates :observations, length: { in: 0..4000 }
  validates :payment_date, presence: :true, :if => :is_payment_type_set?
  validates :discount, presence: :true, :if => :is_valid_discount?

  has_many :service_order_item, :dependent => :destroy
  accepts_nested_attributes_for :service_order_item, :allow_destroy => true

  def is_payment_type_set?
    if(payment_type != 7 and payment_type != 9)
      return true
    end
    return false
  end

  def is_valid_discount?
    if(discount.nil?)
      return 0
    end
    return discount
  end

  scope :search_os, ->(params){
  	
  	where_clause = String.new

    if(params)
      if(!params[:id_os].blank?)
        where_clause = " service_orders.id = " << params[:id_os] << " AND"
      end
    	if(!params[:customer_id].blank?)
    		where_clause = " service_orders.customer_id = " << params[:customer_id] << " AND"
    	end
    	if(!params[:vehicle].blank? and !params[:vehicle].gsub("Selecione...","").blank?)
    		where_clause += " service_orders.vehicle_id = " << params[:vehicle] << " AND"
    	end

    	#ifs de payment date
    	if(!params[:payment_date_de].blank? and !params[:payment_date_ate].blank? )
    		where_clause += " service_orders.payment_date between '" << Date.strptime(params[:payment_date_de],"%d/%m/%Y").to_s() << "' and '" << Date.strptime(params[:payment_date_ate],"%d/%m/%Y").to_s() << "' AND"
    	end

    	#se somente preenchido payment date de
    	if(!params[:payment_date_de].blank? and params[:payment_date_ate].blank? )
    		where_clause += " service_orders.payment_date <= '" << Date.strptime(params[:payment_date_de],"%d/%m/%Y").to_s() << "' AND"
    	end

    	#se somente preenchido payment date ate
    	if(params[:payment_date_de].blank? and !params[:payment_date_ate].blank? )
    		where_clause += " service_orders.payment_date >= '" << Date.strptime(params[:payment_date_ate],"%d/%m/%Y").to_s() << "' AND"
    	end

    	#ifs de created_at
    	if(!params[:created_date_de].blank? and !params[:created_date_ate].blank? )
    		where_clause += " service_orders.created_at between '" << Date.strptime(params[:created_date_de],"%d/%m/%Y").to_s() << "' and '" << Date.strptime(params[:created_date_ate],"%d/%m/%Y").to_s() << "' AND"
    	end

    	#se somente preenchido created_at de
    	if(!params[:created_date_de].blank? and params[:created_date_ate].blank? )
    		where_clause += " service_orders.created_at <= '" << Date.strptime(params[:created_date_de],"%d/%m/%Y").to_s() << "' AND"
    	end

    	#se somente preenchido created_at ate
    	if(params[:created_date_de].blank? and !params[:created_date_ate].blank? )
    		where_clause += " service_orders.created_at >= '" << Date.strptime(params[:created_date_ate],"%d/%m/%Y").to_s() << "' AND"
    	end 

    	#se os foi criada
    	if(!params[:os_created].blank?)
    		where_clause += " service_orders.os_created = " << (params[:os_created] == "false" ? '0' : '1') << " AND"
    	end

    	#os pagas
    	if(!params[:paid].blank?)
    		#where_clause += " service_orders.payment_date " << (params[:paid] == "false" ? 'IS NULL' : ' IS NOT NULL') << " AND"
        where_clause += " service_orders.paid =" << (params[:paid] == "false" ? '0' : '1') << " AND"
    	end

    	#os pagas
    	if(!params[:help].blank?)
    		where_clause += " service_orders.help = " << (params[:help] == "false" ? '0' : '1') << " AND"    		
    	end
    end
    #expressão para remover ultimo AND ou AND)
    where(where_clause.gsub(/((AND\))$|(AND))$/,""))
  }

  scope :by_vehicle, lambda { |vehicle|
    return scoped unless (vehicle.present?) 
    where(:vehicle_id => vehicle)
  }

  scope :paid, lambda { |paid|

    if(paid[:paid].nil? or paid[:paid].blank?)
      return where("")
    else
      return where(:paid => paid[:paid].to_s == "true" ? true : false )    
    end    
  }

  scope :months_total, ->(data_de, data_ate){
    where( " DATE_FORMAT(payment_date,'%Y-%m') between '#{data_de}' and '#{data_ate}' " )
  }
end
