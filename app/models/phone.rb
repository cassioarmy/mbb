class Phone < ActiveRecord::Base
  belongs_to :customer

  before_save :treat_number
  private
  		def treat_number
  			self.number = self.number.gsub(/\D/,'').upcase
  		end
end
