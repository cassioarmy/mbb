class Company < ActiveRecord::Base

	belongs_to :user

	validates :user_id, presence: :true
	validates :social_name, presence: :true
	validates :fantasy_name, presence: :true
	validates :address, presence: :true
	validates :address_number, format: /[0-9]*/
	validates :address_number, numericality: { only_integer: true, allow_blank: true }
	validates :ie, numericality: { only_integer: true, allow_blank: true }
	validates :im, numericality: { only_integer: true, allow_blank: true }
	validates :email, format: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

end
