class ServiceOrderItem < ActiveRecord::Base
	belongs_to :service_order
	
	belongs_to :service_order_item_description

	validates :service_order_item_description_id, presence: :true
	validates :soi_date, presence: :true
	
end
