class ServiceOrderItemDescription < ActiveRecord::Base

	validates :description, presence: :true

end
