class Vehicle < ActiveRecord::Base
	self.per_page = 10

  	belongs_to :state
  	belongs_to :city
    belongs_to :user

    has_many :customer_vehicle, autosave: true, dependent: :destroy
    has_many :customervehicle, through: :customer_vehicle
    accepts_nested_attributes_for :customer_vehicle, allow_destroy: true
  	# has_and_belongs_to_many :customer

  	before_save :treat_plate, :upper_vehicle

    validates :user_id, presence: :true
    validates :description, presence: :true
    validates :license_plate, presence: :true
    # validates :year_model, presence: :true

  	private
  		def treat_plate
  			self.license_plate = self.license_plate.gsub("-","")
  			self.year_model = self.year_model.gsub("-","")
  		end

      def upper_vehicle
        self.chassi = self.chassi.upcase
        self.license_plate = self.license_plate.upcase
      end
end
