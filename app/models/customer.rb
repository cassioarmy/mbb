class Customer < ActiveRecord::Base
 # attr_accessible :id, :name, :address, :number, :neighborhood, :city, :cep, :cpf_cnpj, :email
  belongs_to :city
  belongs_to :user

  has_many :phones
  has_many :state_registrations
  has_many :customer_vehicle, dependent: :destroy
  has_many :customervehicle, through: :customer_vehicle, :source => :vehicle

  accepts_nested_attributes_for :customer_vehicle, allow_destroy: true
  accepts_nested_attributes_for :phones, allow_destroy: true
  accepts_nested_attributes_for :state_registrations, allow_destroy: true

  validates :user_id, presence: :true
  validates :name, presence: :true
  validates :cpf_cnpj, presence: :true

  before_save :treat_cpf_cnpj

  private
  		def treat_cpf_cnpj
  			self.cpf_cnpj = self.cpf_cnpj.gsub(/\D/,'').upcase
        self.cep = self.cep.gsub(/\D/,'').upcase
  		end
end
