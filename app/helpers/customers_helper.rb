module CustomersHelper
	
	def format_phone_number(phone_number)
		phone_formatter =  phone_number

		if phone_number and phone_number.length == 10
			phone_formatter = phone_number.gsub(/\A(\d{2})(\d{4})(\d{4})\Z/, "\(\\1\)\\2-\\3")
		elsif phone_number and phone_number.length == 11			
			phone_formatter = phone_number.gsub(/\A(\d{2})(\d{5})(\d{4})\Z/, "\(\\1\)\\2-\\3")
		end
			
		phone_formatter 
	end

	def format_cpf_cnpj(cpf_cnpj)
		cpf_cnpj_formatter = cpf_cnpj

		if cpf_cnpj and cpf_cnpj.length == 11
			cpf_cnpj_formatter = cpf_cnpj.gsub(/\A(\d{3})(\d{3})(\d{3})(\d{2})\Z/, "\\1.\\2.\\3-\\4")
		elsif cpf_cnpj and cpf_cnpj.length == 14
			cpf_cnpj_formatter = cpf_cnpj.gsub(/\A(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})\Z/, "\\1.\\2.\\3/\\4-\\5")
		end
			
		cpf_cnpj_formatter 
	end

	def format_cep(cep)
		cep_formatter = cep

		if cep and cep.length == 8
			cep_formatter = cep.gsub(/\A(\d{5})(\d{3})\Z/, "\\1-\\2")		
		end
		cep_formatter 
	end

	def load_cities(state)
		City.where("state_id = ?","#{state}")
	end

	def get_phones(customer)

		phones = ""

		if (not customer.nil? and customer.phones.length > 0)
			customer.phones.each do |p|
				phones += format_phone_number(p.number)
			end
		end

		phones
	end
end
