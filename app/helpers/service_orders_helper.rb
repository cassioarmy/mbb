module ServiceOrdersHelper

	def format_status_os_busca_class(status)
		if(status.blank?)
			return "btn-info";
		end
		
		if(status == "true")
			"btn-success"
		else
			"btn-danger"
		end
	end

	def format_status_os_busca(status)
		if(status.blank?)
			return "Todos";
		end

		if(status == "true")
			"Sim"
		else
			"Não"
		end
	end

	def format_status_os(status)
		if(status)
			"Sim"
		else
			"Não"
		end
	end

	def format_status_os_class(status)
		if(status)
			"btn-success"
		else
			"btn-danger"
		end
	end

	def tipo_pagamento_desc(tp_pagamento_code)
		desc = ""
		case tp_pagamento_code
		when 1
			desc = "À Vista"
		when 2
			desc = "Cheque"
		when 3
			desc = "Cartão"
		when 4
			desc = "30 Dias"
		when 5
			desc = "60 Dias"
		when 6
			desc = "90 Dias"
		when 7
			desc = "Não Pago"
		when 8
			desc = "Boleto"
		when 9
			desc = "Orçamento"
		else
			desc="Não Informada"
		end
	end
end
