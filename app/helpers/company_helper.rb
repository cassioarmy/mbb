module CompanyHelper

	def h_address(company)
		address = ""

		if (company.address)
			address += company.address
		end

		if (company.address_number)
			address += ", " + company.address_number
		end

		if (company.address_neighborhood)
			address += " - " + company.address_neighborhood
		end

		address
	end

end
