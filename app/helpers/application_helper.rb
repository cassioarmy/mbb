module ApplicationHelper

	def controller?(*controller)
		controller.include?(params[:controller])
	end

	def action?(*action)
		action.include?(params[:action])
	end

	def number_to_currency_unit(number)
  		number_to_currency(number, :unit => "R$ ", :separator => ",", :delimiter => ".")
	end

	def number_to_currency_br(number)
  		number_to_currency(number, :unit => "", :separator => ",", :delimiter => ".")
	end

	def format_date(data)
		if !data.nil?
			data.strftime("%d/%m/%Y")
		end
	end



	def format_date_extenso(data)
		if !data.nil?
			data.strftime("%d %B %Y")
		end
	end
end
