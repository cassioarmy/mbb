module VehiclesHelper

	def format_license_plate(license_plate)
		license_plate_formatted = license_plate

		if (license_plate and license_plate.length >= 7)
			license_plate_formatted = license_plate[0,3] +"-"+ license_plate[3,6]
			license_plate_formatted = license_plate_formatted.upcase
		end

		license_plate_formatted
	end

	def format_year_model(year_model)
		year_model_formatted = year_model

		if (year_model and year_model.length >= 8)
			year_model_formatted = year_model[0,4] +"-"+ year_model[4,7]
		end

		year_model_formatted
	end

	def format_status(status)
		if(status and status == '1')
			"Ativo"
		else
			"Inativo"
		end
	end

	def format_status_class(status)
		if(status and status == '1')
			"btn-success"
		else
			"btn-danger"
		end
	end
end
