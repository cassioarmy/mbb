module ServiceOrderItemDescriptionHelper
	def item_description(id)
		if(id)
			ServiceOrderItemDescription.find(id)
		else
			ServiceOrderItemDescription.new
		end
	end
end
