class ServiceOrderPolicy < ApplicationPolicy

  attr_reader :user, :service_order

  def initialize(user, service_order)
    @user = user
    @service_order = service_order
  end

  def index?
    admin_role? or supervisor_role?
  end

  def show?
    admin_role? or supervisor_role?
  end

  def create?
    admin_role? or supervisor_role?
  end

  def new?
    create?
  end

  def update?
    admin_role? or supervisor_role?
  end

  def edit?
    update?
  end

  def destroy?
    admin_role? or supervisor_role?
  end

  def scope
    Pundit.policy_scope!(user, service_order.class)
  end

end