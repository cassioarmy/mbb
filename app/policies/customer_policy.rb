class CustomerPolicy < ApplicationPolicy

  attr_reader :user, :customer

  def initialize(user, customer)
    @user = user
    @customer = customer
  end

  def scope
    Pundit.policy_scope!(user, customer.class)
  end

end