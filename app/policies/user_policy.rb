class UserPolicy < ApplicationPolicy

  attr_reader :user, :user_model

  def initialize(user, user_model)
    @user = user
    @user_model = user_model
  end

  def index?
    admin_role? or supervisor_role?
  end

  def show?
    current_user?
  end

  def create?
    admin_role? or supervisor_role?
  end

  def new?
    create?
  end

  def update?
    current_user?
  end

  def edit?
    update?
  end

  def destroy?
    admin_role? or supervisor_role?
  end

  def scope
    Pundit.policy_scope!(user, user_model.class)
  end

  def permitted_attributes
    if admin_role? or supervisor_role?
      [:email, :full_name, :phone, :cel_phone, :password, :password_confirmation, :roles => [], :role_ids => []]
    elsif(current_user?)
      [:full_name, :phone, :cel_phone, :password, :password_confirmation]
    end
  end

  private 

    def current_user?
      user.email == user_model.email or admin_role? or supervisor_role?
    end

end