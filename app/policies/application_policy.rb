class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    all_roles?
  end

  def show?
    all_roles?
  end

  def create?
    all_roles?
  end

  def new?
    create?
  end

  def update?
    all_roles?
  end

  def edit?
    update?
  end

  def destroy?
    all_roles?
  end

  def show_user_column?
    admin_role? and supervisor_role?
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  protected
    def admin_role?
      @user.role?("Admin")
    end

    def operator_role?
      @user.role?("Operador")
    end

    def supervisor_role?
      @user.role?("Supervisor")
    end

  private 

    def all_roles?
      admin_role? || operator_role? || supervisor_role?
    end
end

