class CompanyPolicy < ApplicationPolicy

  attr_reader :user, :company

  def initialize(user, company)
    @user = user
    @company = company
  end

  def index?
    admin_role? or supervisor_role?
  end

  def show?
    admin_role? or supervisor_role?
  end

  def create?
    admin_role? or supervisor_role?
  end

  def new?
    create?
  end

  def update?
    admin_role? or supervisor_role?
  end

  def edit?
    update?
  end

  def destroy?
    admin_role? or supervisor_role?
  end

  def scope
    Pundit.policy_scope!(user, company.class)
  end

end