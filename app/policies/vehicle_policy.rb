class VehiclePolicy < ApplicationPolicy

  attr_reader :user, :vehicle

  def initialize(user, vehicle)
    @user = user
    @vehicle = vehicle
  end

  def scope
    Pundit.policy_scope!(user, vehicle.class)
  end

end