class ApplicationMailer < ActionMailer::Base
  default from: "mecanicabalsamobenz@gmail.com"

  def send_so(service_order, so_file, nf_file)
  	@company = Company.first
  	@service_order = service_order
  	attachments.inline['logo.jpg'] = get_logo()
    unless (nf_file.nil?)
  	  attachments.inline['nota_fiscal.pdf'] = nf_file
    end
  	attachments.inline[get_so_filename(@service_order)] = so_file

  	delivery_options = {
  		user_name: "mecanicabalsamobenz@gmail.com",
  		password: "ac32641088",
  		address: "smtp.gmail.com",
  		domain: "gmail.com",
  		port: 587,
  		authentication: 'plain',
    	enable_starttls_auto: true
  	}

  	mail(to: @service_order.customer.email, 
  		subject: (@service_order.payment_type == 9 ? "Emissão de orçamento" : "Emissão de ordem de serviço"), 
  		delivery_method_options: delivery_options)
  end

  private 

  	def get_logo
  		File.read('app/assets/images/logo-for-report.jpg')
  	end

  	def get_so_filename(service_order)
  		"ordem_servico_" + service_order.id.to_s + ".pdf"
  	end

end
