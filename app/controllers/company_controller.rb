class CompanyController < ApplicationController

  before_action :set_company, only: [:show, :edit, :update]

  before_action :authenticate_user!

  after_action :verify_authorized

  layout "main_application"

  def edit
  	if (@company.nil?)
  		@company = Company.new
  	end

    authorize @company
  end

  def show

    if (@company.nil?)
      @company = Company.new
      redirect_to edit_company_path
    end

    authorize @company
  end

  # POST /company
  # POST /company.json
  def create
    @company = Company.new(company_params)
    
    authorize @company

    @company.user = current_user

    respond_to do |format|
      if @company.save
        format.html { redirect_to @company, notice: 'Dados da empresa atualizados com sucesso.' }
        format.json { render :show, status: :created, location: @company }
      else
        format.html { render :edit }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company/1
  # PATCH/PUT /company/1.json
  def update
    authorize @company

    @company.user = current_user
    
    respond_to do |format|
      if @company.update(company_params)
        format.html { redirect_to @company, notice: 'Dados da empresa atualizados com sucesso.' }
        format.json { render :show, status: :ok, location: @company }
      else
        format.html { render :edit }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:email, :social_name, :fantasy_name, :ie, :cnpj, :im, :address, :address_number, :address_neighborhood, :observation, :phone, :phone2)
    end
end
