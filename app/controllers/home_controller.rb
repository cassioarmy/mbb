class HomeController < ApplicationController
  
  layout "main_application"
  
  before_action :authenticate_user!
  
  def index
  	@customer_count = Customer.count
  	@vehicle_count = Vehicle.count
  	@user_count = User.count
  	@service_order_count = ServiceOrder.count
	
	data_de = ""
  	data_ate = ""
  	puts params[:data_de]
  	if(params[:data_de].blank?)
  		params[:data_de] = Time.now.year - 1;
  		data_de = (Time.now.year - 1).to_s+"-01"
	else
		data_de = "#{params[:data_de]}-01"
	end

	if(params[:data_ate].blank?)
		params[:data_ate] = Time.now.year;
		data_ate = (Time.now.year).to_s+"-12"
	else
		data_ate = "#{params[:data_ate]}-12"
	end
	
  	generate_monts_total_graphs(data_de, data_ate)	

  end

  private
  	def generate_monts_total_graphs(d1,d2)
  		
  		months_totals = ServiceOrder.select("DATE_FORMAT(payment_date,'%m') mes, DATE_FORMAT(payment_date,'%Y') p_ano,sum(unit_price) total")
  				.joins(:service_order_item)
  				.months_total( d1 , d2)
  				.paid(params)
  				.group("DATE_FORMAT(service_orders.payment_date,'%m/%Y')")
  				.order("DATE_FORMAT(service_orders.payment_date,'%m/%Y')")

	  	param1 = d1[0..4]#pega o ano
		param2 = d2[0..4]#pega somente o ano

		h_series = Hash.new 
		
		meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro']
		adicionou = false
		(param1.to_i..param2.to_i).each do |e|
			itn = Array.new
			meses.each_index{|mes_each|	  		 
	  			itn.push(0)
	  			months_totals.each { |item| 	
					#senao tiver valor no mes coloca 0	  		
	  		 		if(item.p_ano.to_i == e.to_i and item.mes.to_i == (mes_each+1).to_i)
	  		 			itn[mes_each] = ( item.total.to_f )	  		 	
	  		 		end
	  		 	}
	  		}
	  		
	  		h_series[e.to_s] = itn
	  	end
	  	puts h_series.to_s
	  	@chart = LazyHighCharts::HighChart.new('graph') do |f|
	  	  f.options[:xAxis][:categories] = meses
	  	  f.options[:tooltip][:pointFormat] = "Valor: R$ {point.y:.2f}"

	  	  #adicionando series
	  	  h_series.each { |k,v|
	  	  	f.series(:name=> "Ano #{k}",:data=> v)
	  	  }
	      #f.series(:name=> 'Ano 2013',:data=> [1100, 1300, 1100, 1500, 900, 5000, 3000,9000,6000,4000,2000,7000 ])
	      #f.series(:name=>'Jane',:data=>[1, 3, 4, 3, 3, 5, 4,46] )     
	      f.title({ :text=>"Valores de Ordem de Serviços no Período"})

	      ###  Options for Bar
	      ### f.options[:chart][:defaultSeriesType] = "bar"
	      # f.plot_options({:series=>{:stacking=>"normal"}}) 
	      
	      ## or options for column
	      #f.options[:chart][:defaultSeriesType] = "column"
	      #f.plot_options({:column=>{:stacking=>"percent"}})
	      f.chart({:defaultSeriesType=>"column"})
	    end
	end
end
