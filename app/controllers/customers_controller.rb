class CustomersController < ApplicationController
  before_action :set_customer, only: [:show, :edit, :update, :destroy]
  
  before_action :authenticate_user!
  
  layout "main_application"
  # GET /customers
  # GET /customers.json
  def index
    @customers = Customer.all.order("name").paginate(:page => params[:page], :per_page => 10)
  end

  def find

    order_by_param = "name"

    if (!params[:order_by_param].blank?)
      order_by_param = params[:order_by_param]
    end

    per_page_param = 10

    if (params[:per_page_param])
      per_page_param = params[:per_page_param]
    end

    @customers = Customer.where("name LIKE ? AND cpf_cnpj LIKE ?", "%#{params[:name]}%", "%"+"#{params[:cpf_cnpj]}%".gsub(/\D/,'')+"%").order(order_by_param).paginate(:page => params[:page], :per_page => per_page_param)

    respond_to do |format|
        format.html { render :index }
        format.json { 
          render json: {
            customers: @customers.as_json(:include => [:phones, :customervehicle]), 
            pagination: {
              total_pages: @customers.total_pages, 
              current_page: @customers.current_page, 
              total_entries: @customers.total_entries
            } 
          } 
        }
    end
  end

  # GET /customers/1
  # GET /customers/1.json
  def show
  end

  # GET /customers/new
  def new
    @customer = Customer.new
    @id_city = ""
    @id_state = ""
    @cities = City.none
    @vehicles = Vehicle.none
    @phones = Phone.none
  end

  # GET /customers/1/edit
  def edit

    if( @customer.city )
      @id_city = @customer.city.id
      @id_state = @customer.city.state.id
    end


    
    @vehicles = @customer.customer_vehicle

    @phones = @customer.phones
    

    load_cities_from_state()
  end

  # POST /customers
  # POST /customers.json
  def create
    @customer = Customer.new(customer_params)

    @customer.user = current_user

    respond_to do |format|
      if @customer.save
        format.html { redirect_to @customer, notice: 'Cliente criado com sucesso.' }
        format.json { render :show, status: :created, location: @customer }
      else
        format.html { render :new }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end

      @cities = []
      @vehicles = @customer.customer_vehicle

      @phones = @customer.phones
      if (@customer.city)
        @cities = load_cities(@customer.city.state_id)
      end

    end

   

  end

  # PATCH/PUT /customers/1
  # PATCH/PUT /customers/1.json
  def update
    respond_to do |format|

      @customer.user = current_user

      if @customer.update(customer_params)

        if(params[:removerVeiculos])
          vlr = params[:removerVeiculos].split(",");
          
          vlr.each do |item|  
            @customerVehicle = CustomerVehicle.find(item);
            @customerVehicle.destroy
          end
        end
        format.html { redirect_to @customer, notice: 'Cliente atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @customer }
      else
        format.html { render :edit }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end

      @cities = []
      @vehicles = @customer.customer_vehicle

      @phones = @customer.phones
      if (@customer.city)
        @cities = load_cities(@customer.city.state_id)
      end
    end

    
  end

  def cities_from_state
    @cities = City.joins(:state).where("states.id = ? ", params[:state_id])

    respond_to do |format|
      format.json { render json: @cities, :status => :created}
    end
  end
  
  def search_vehicle
     @vehicles = Vehicle.joins("LEFT JOIN customer_vehicles ON customer_vehicles.vehicle_id = vehicles.id")
     .where("vehicles.status = '1' AND (customer_id is null or customer_id <> ?) AND description LIKE ? AND license_plate LIKE ?","#{params[:customerid]}", "%#{params[:description]}%", "%#{params[:license_plate]}%".gsub("-","")).order("description").paginate(:page => params[:page], :per_page => 5)
     #,"#{params[:customerid]}"

    respond_to do |format|
      format.html {render partial: 'search_vehicle', locals: {:vehicles => @vehicles} }
      format.json {render json: @vehicles, :status => :created}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      @customer = Customer.find(params[:id])
    end

    def load_cities(stateId)
      City.joins(:state).where("states.id = ? ", stateId)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customer_params
      params.require(:customer).permit(:id,:name,:address,:address2,:number,:neighborhood,:cep,:email,:city_id,:cpf_cnpj,:status,
                      :customer_vehicle_attributes => [:vehicle_id, :customer_id],:phones_attributes =>[:id,:number,:tp,:contact,:provider,:_destroy],
                      :state_registrations_attributes => [:id,:number,:description,:city_id,:_destroy])
    end

    def load_cities_from_state

      if(@customer.city)
        @cities = City.joins(:state).where("states.id = ? ", @customer.city.state.id)
      else
        @cities = City.none
      end

    end
end
