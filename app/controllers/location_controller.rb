class LocationController < ApplicationController

	before_action :authenticate_user!

	def cities_from_state
		cities = City.joins(:state).where("states.id = ? ", params[:state_id])

	    respond_to do |format|
	      format.json { render json: cities, :status => :created}
	    end
	end

	def states
      @states = State.all
      
      respond_to do |format|      
      	format.json { render json: @states, :status => :created }
      end
  	end

end
