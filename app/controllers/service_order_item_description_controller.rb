class ServiceOrderItemDescriptionController < ApplicationController
  before_action :authenticate_user!

  def create     
  	@service_order_item_description = ServiceOrderItemDescription.new  	
  	@service_order_item_description.description = params[:description]

    achou_cadastrado = ServiceOrderItemDescription.where("description = ?", "#{params[:description]}")

  	respond_to do |format|

      #senão achou cadastra
      if( achou_cadastrado.length == 0 )
        if @service_order_item_description.save	        
          format.json { render json: @service_order_item_description, status: :created }
        else	        
          format.json { render json: @service_order_item_description.errors, status: :unprocessable_entity }
        end
      else
        #senão retorna o cadastrado
        format.json { render json: achou_cadastrado[0], status: :created }
 	    end
    end
  end



  #def description_items    
  #  @descs = []
  #  descriptions = ServiceOrderItemDescription.where("description like ?","%#{params['q']}%").paginate(:page => params[:page], :per_page => 10)
           
  #  puts params.inspect

  # @descs.push({id:params["q"], description: params["q"]})
  #  @descs.concat(descriptions)

  # respond_to do |format|       
  #     format.json { render json: @descs, status: :created }
  #  end

  #end


  def description_items    
    @descs = []
    descriptions = ServiceOrderItemDescription.select('description as label','id as value').where("description like ?","%#{params['term']}%").paginate(:page => params[:page], :per_page => 10)
           
    puts params.inspect

    @descs.push({description:params["term"], value: params["term"]})
    @descs.concat(descriptions)

    respond_to do |format|       
       format.json { render json: @descs }
    end

  end
  
end
