class VehiclesController < ApplicationController
  
  before_action :authenticate_user!
  before_action :set_vehicle, only: [:show, :edit, :update, :destroy]

  layout "main_application"
  # GET /vehicles
  # GET /vehicles.json
  def index
    @vehicles = Vehicle.all.order("description").paginate(:page => params[:page])
  end

  # GET /vehicle/find
  def find
    @vehicles = Vehicle.where("description LIKE ? AND license_plate LIKE ?", "%#{params[:description]}%", "%#{params[:license_plate]}%".gsub("-","")).order("description").paginate(:page => params[:page])

    respond_to do |format|
        format.html { render :index }
    end
  end

  # GET /vehicles/1
  # GET /vehicles/1.json
  def show
  end

  # GET /vehicles/new
  def new
    @vehicle = Vehicle.new 
    @vehicle.status = '1';
    @cities = []
  end

  # GET /vehicles/1/edit
  def edit
    @vehicle = Vehicle.find(params[:id])
    @cities = []

    if (@vehicle.city)
      @cities = load_cities(@vehicle.city.state_id)
    end
  end

  # POST /vehicles
  # POST /vehicles.json
  def create
    @vehicle = Vehicle.new(vehicle_params)

    @vehicle.user = current_user

    respond_to do |format|
      if @vehicle.save
        format.html { redirect_to @vehicle, notice: 'Veículo criado com sucesso.' }
        format.json { render :show, status: :created, location: @vehicle }
      else
        format.html { render :new }
        format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end

      @cities = []

      if (@vehicle.city)
        @cities = load_cities(@vehicle.city.state_id)
      end
    end
  end

  # PATCH/PUT /vehicles/1
  # PATCH/PUT /vehicles/1.json
  def update

    @vehicle.user = current_user
    
    respond_to do |format|
      if @vehicle.update(vehicle_params)
        format.html { redirect_to @vehicle, notice: 'Veículo atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @vehicle }
      else
        format.html { render :edit }
        format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end

      @cities = []

      if (@vehicle.city)
        @cities = load_cities(@vehicle.city.state_id)
      end
    end
  end

  def cities_from_state
    @cities = load_cities(params[:state_id])

    respond_to do |format|
      format.json { render json: @cities, :status => :created}
    end
  end

  private
    
    def load_cities(stateId)
      City.joins(:state).where("states.id = ? ", stateId)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_vehicle
      @vehicle = Vehicle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vehicle_params
      params.require(:vehicle).permit(:description, :city, :license_plate, :year_model, :chassi, :city_id, :status)
    end

end
