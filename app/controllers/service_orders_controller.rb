# encoding: utf-8

class ServiceOrdersController < ApplicationController

  before_action :set_service_order, only: [:show, :edit, :update, :destroy, :report]

  before_action :authenticate_user!

  # after_action :verify_authorized

  layout "main_application"
  
  # GET /service_orders
  # GET /service_orders.json
  def index
    @customer = Customer.new

    per_page_param = 10

    if (params[:per_page_param])
      per_page_param = params[:per_page_param]
    end

    @service_orders = ServiceOrder.select("service_orders.id,service_orders.created_at,customer_id,vehicle_id,payment_date,os_created,order_date,help,payment_type,sum(service_order_items.unit_price) as subtotal,sum(service_order_items.unit_price) - service_orders.discount as total,service_orders.discount as discount,service_orders.paid,user_id")
    .joins(:service_order_item).group("service_order_items.service_order_id").order("service_orders.created_at DESC")
    .order("service_orders.created_at DESC")
    .paginate(:page => params[:page], :per_page => per_page_param)


  end

  # GET /service_orders/1
  # GET /service_orders/1.json
  def show

    @company = Company.first

    respond_to do |format|
      format.html

      format.pdf do 
        render :pdf  => "ordem_servico_" + @service_order.id.to_s
      end
    end

  end

  # GET /service_orders/new
  def new
    @service_order = ServiceOrder.new
    @service_order.os_created = false
    @service_order.help = false
    @service_order.service_order_item.build
    @vehicles = []
  end

  # GET /service_orders/1/edit
  def edit
    vehicle_of_customer
  end

  # POST /service_orders
  # POST /service_orders.json
  def create
    @service_order = ServiceOrder.new(service_order_params)

    @service_order.user = current_user

    respond_to do |format|
      if @service_order.save
        format.html { redirect_to @service_order, notice: 'Ordem de Serviço criada com sucesso' }
        format.json { render :show, status: :created, location: @service_order }
      else
        vehicle_of_customer
        format.html { render :new }
        format.json { render json: @service_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /service_orders/1
  # PATCH/PUT /service_orders/1.json
  def update

    @service_order.user = current_user
    
    respond_to do |format|
      if @service_order.update(service_order_params)
        format.html { redirect_to @service_order, notice: 'Ordem de serviço atualizada com sucesso' }
        format.json { render :show, status: :ok, location: @service_order }
      else
        vehicle_of_customer
        format.html { render :edit }
        format.json { render json: @service_order.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy

    ServiceOrder.find(params[:id]).destroy;

    if(!params[:customer_id].blank?)
      @customer = Customer.find(params[:customer_id])      
    end


    per_page_param = 10

    if (params[:per_page_param])
      per_page_param = params[:per_page_param]
    end

     @service_orders = ServiceOrder.select("service_orders.id,service_orders.created_at,customer_id,vehicle_id,payment_date,os_created,order_date,help,payment_type,,sum(service_order_items.unit_price) as subtotal,sum(service_order_items.unit_price)  - service_orders.discount as total,service_orders.discount as discount,service_orders.paid,user_id")
    .joins(:service_order_item)
    .search_os(params)
    .group("service_order_items.service_order_id").order("service_orders.created_at DESC")
    .paginate(:page => params[:page], :per_page => per_page_param)

    respond_to do |format|
        format.html { redirect_to service_orders_path, notice: 'Ordem de serviço excluida com sucesso' }
    end
  end

  def find
    
    @customer = Customer.new

    if(!params[:customer_id].blank?)
      @customer = Customer.find(params[:customer_id])      
    end

    per_page_param = 10

    if (params[:per_page_param])
      per_page_param = params[:per_page_param]
    end

     @service_orders = ServiceOrder.select("service_orders.id,service_orders.created_at,customer_id,vehicle_id,payment_date,os_created,order_date,help,payment_type,sum(service_order_items.unit_price) as subtotal,sum(service_order_items.unit_price)  - service_orders.discount as total,service_orders.discount as discount,service_orders.paid,user_id")
    .joins(:service_order_item)
    .search_os(params)
    .group("service_order_items.service_order_id").order("service_orders.created_at DESC")
    .paginate(:page => params[:page], :per_page => per_page_param)

    respond_to do |format|        
        format.html { render :index }
    end

  end

  #def description_items
  #  estados = [id:params["q"], name: params["q"]]
  #  respond_to do |format|       
  #     format.json { render json: estados, status: :created }
  #  end
  #end

  def description_items
    estados = [id:params["q"], name: params["q"]]
    debug(params)
    respond_to do |format|       
       format.json { render json: estados, status: :created }
    end
  end

  def report
    @company = Company.first
    render layout: "report_application"
  end

  # POST
  def send_mail

    @service_order = ServiceOrder.find(params[:so_id])
    @company = Company.first

    nf_file = nil
    
    if (params[:nf_file])
      nf_file = params[:nf_file].read
    end

    so_file = WickedPdf.new.pdf_from_string(render_to_string('service_orders/show.pdf.erb'))

    ApplicationMailer.send_so(@service_order, so_file, nf_file).deliver()

    email_log = EmailLog.new
    email_log.email_from = "mecanicabalsamobenz@hotmail.com"
    email_log.service_order = @service_order

    respond_to do |format|
      if email_log.save
        format.html { redirect_to service_orders_path, notice: "Ordem de Serviço número #{params[:so_id]} enviada por email"  }
      else
        format.html { redirect_to service_orders_path, alert: "Erro ao enviar Ordem de Serviço número #{params[:so_id]} por email" }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_order
      @service_order = ServiceOrder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_order_params
      params.require(:service_order).permit(:customer_id, :vehicle_id, :payment_date, :os_created, :order_date, :payment_type, :help,:paid,:observations,:km_service,:discount,:_destroy, 
          :service_order_item_attributes => [:id,:soi_date, :service_order_item_description_id,:unit_price,:_destroy])
    end

    def vehicle_of_customer 
      @vehicles =  Vehicle.joins("INNER JOIN customer_vehicles ON customer_vehicles.vehicle_id = vehicles.id")
      .where("vehicles.status = '1' AND (customer_id = ?)",@service_order.customer_id)

      vehicle = Vehicle.new
      vehicle.id = ""
      vehicle.license_plate = ""
      vehicle.description="Selecione...."
      @vehicles.append(vehicle)
    end
end
