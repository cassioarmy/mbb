json.array!(@service_orders) do |service_order|
  json.extract! service_order, :id, :customer, :vehicle_id, :payment_date, :os_created, :order_date, :paid
  json.url service_order_url(service_order, format: :json)
end
