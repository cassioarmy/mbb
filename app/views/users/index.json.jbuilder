json.array!(@users) do |user|
  json.extract! user, :id, :email, :full_name, :phone, :cel_phone
  json.url user_url(user, format: :json)
end
