json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :description, :city, :license_plate, :year_model, :chassi
  json.url vehicle_url(vehicle, format: :json)
end
