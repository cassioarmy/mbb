#encoding:UTF-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#Estado.create([{:uf => "SP", :nome => "São Paulo"}])

roles = Role.create([
	{name: "Admin"},
	{name: "Operador"},
	{name: "Supervisor"}
])

User.create([
	{email: 'luizrobertofreitas@gmail.com', password: "12345678", full_name: "Luiz Freitas", cel_phone: "17 99000-0000", roles: Role.all_but_admin},
	{email: 'admin@admin.com', password: "12345678", full_name: "Administrador", cel_phone: "17 99000-0000", roles: roles},
	{email: 'cassio.army@gmail.com', password: "12345678", full_name: "Cassio Sousa", cel_phone: "17 99000-0000", roles: Role.all_but_admin}
])


