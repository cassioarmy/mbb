class CreateStateRegistrations < ActiveRecord::Migration
  def change
    create_table :state_registrations do |t|
      t.string :number, limit: 50
      t.references :customer, index: true

      t.timestamps
    end
  end
end
