class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.string :description, :limit => 100
      t.references :city
      t.string :license_plate, :limit => 7
      t.string :year_model, :limit => 8
      t.string :chassi, :limit => 17
      
      t.timestamps
    end
  end
end
