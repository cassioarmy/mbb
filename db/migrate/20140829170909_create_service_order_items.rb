class CreateServiceOrderItems < ActiveRecord::Migration
  def change
    create_table :service_order_items do |t|
      t.date :soi_date      
      t.decimal :unit_price, :null => false, :precision => 10, :scale => 2
      t.references :service_order_item_description, :null => false
      t.references :service_order, :null => false

      t.timestamps
    end
  end
end
