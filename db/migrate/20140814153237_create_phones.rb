class CreatePhones < ActiveRecord::Migration
  def change
    create_table :phones do |t|
      t.string :ddd, limit: 3
      t.string :number, limit: 11
      t.string :tp, limit: 10
      t.string :contact, limit: 50
      t.string :provider, limit: 10
      t.references :customer, index: true

      t.timestamps
    end
  end
end
