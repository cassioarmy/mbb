class AddDiscountToServiceOrders < ActiveRecord::Migration
  def change
    add_column :service_orders, :discount, :decimal, :null => false, :precision => 10, :scale => 2,:default => 0
  end
end
