class AddColumnStatus < ActiveRecord::Migration
  def change
  	add_column :vehicles, :status, :string, limit: 1, default: 1
  	add_column :customers, :status, :string, limit: 1, default: 1
  end
end
