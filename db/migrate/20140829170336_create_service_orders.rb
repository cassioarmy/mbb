class CreateServiceOrders < ActiveRecord::Migration
  def change
    create_table :service_orders do |t|
      t.references :customer, index: true, null: false
      t.references :vehicle, index: true, null: false
      t.date :payment_date
      t.boolean :nf_created, default: false
      t.date :order_date

      t.timestamps
    end
  end
end
