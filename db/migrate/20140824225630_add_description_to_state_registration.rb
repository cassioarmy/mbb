class AddDescriptionToStateRegistration < ActiveRecord::Migration
  def change
    add_column :state_registrations, :description, :string, limit: 250
    add_column :state_registrations, :city_id, :integer, index: true
  end
end
