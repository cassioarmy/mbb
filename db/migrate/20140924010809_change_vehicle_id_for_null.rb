class ChangeVehicleIdForNull < ActiveRecord::Migration
  def change
  		change_column :service_orders, :vehicle_id, :integer, :null => true
  end
end
