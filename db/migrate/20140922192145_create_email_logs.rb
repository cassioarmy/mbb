class CreateEmailLogs < ActiveRecord::Migration
  def change
    create_table :email_logs do |t|

      t.string :email_from
      t.references :service_order, index: :true

      t.timestamps
    end
  end
end
