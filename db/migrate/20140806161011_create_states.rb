class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.string :name, :limit => 50
      t.string :uf, :limit => 2

      t.timestamps
    end
  end
end
