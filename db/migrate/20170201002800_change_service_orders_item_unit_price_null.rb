class ChangeServiceOrdersItemUnitPriceNull < ActiveRecord::Migration
  def change    
    change_column :service_order_items, :unit_price, :decimal, :null => true
  end
end
