class CreateCustomerVehicles < ActiveRecord::Migration
  def change
    create_table :customer_vehicles do |t|
      t.references :customer, index: true
      t.references :vehicle, index: true

      t.timestamps
    end
  end
end
