class AddPaidToServiceOrders < ActiveRecord::Migration
  def change
    add_column :service_orders, :paid, :boolean, default:false
  end
end
