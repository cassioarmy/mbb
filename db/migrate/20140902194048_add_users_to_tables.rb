class AddUsersToTables < ActiveRecord::Migration
  def change
  	add_column :vehicles, :user_id, :integer, null: :false
  	add_column :customers, :user_id, :integer, null: :false
  	add_column :companies, :user_id, :integer, null: :false
  	add_column :service_orders, :user_id, :integer, null: :false
  end
end
