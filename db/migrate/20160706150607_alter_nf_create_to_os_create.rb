class AlterNfCreateToOsCreate < ActiveRecord::Migration
  def change
  	rename_column :service_orders, :nf_created, :os_created
  end
end
