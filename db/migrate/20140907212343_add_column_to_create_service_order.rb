class AddColumnToCreateServiceOrder < ActiveRecord::Migration
  def change
    add_column :service_orders, :help, :boolean, null: false
    add_column :service_orders, :payment_type, :integer, null: true, limit: 1
  end
end
