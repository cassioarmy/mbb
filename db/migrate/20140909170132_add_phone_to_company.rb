class AddPhoneToCompany < ActiveRecord::Migration
  def change
  	add_column :companies, :phone, :string
  	add_column :companies, :phone2, :string
  end
end
