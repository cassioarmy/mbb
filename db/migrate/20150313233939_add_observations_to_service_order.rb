class AddObservationsToServiceOrder < ActiveRecord::Migration
  def change
    add_column :service_orders, :observations, :string, null: true, limit: 4000
  end
end
