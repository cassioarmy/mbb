class AddKmServiceToServiceOrders < ActiveRecord::Migration
  def change
    add_column :service_orders, :km_service, :string, limit: 250
  end
end
