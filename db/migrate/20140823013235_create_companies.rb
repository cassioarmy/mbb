class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :social_name
      t.string :fantasy_name
      t.string :address
      t.string :address_number
      t.string :address_neighborhood
      t.string :email
      t.string :cnpj
      t.string :ie
      t.string :im
      t.text :observation

      t.timestamps
    end
  end
end
