class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name, limit: 100
      t.string :address, limit: 80
      t.string :number, limit: 4
      t.string :neighborhood, limit: 50 #bairro
      t.references :city, index: true
      t.string :cep, limit: 8
      t.string :cpf_cnpj, limit: 14
      t.string :email, limit: 100

      t.timestamps
    end
  end
end
