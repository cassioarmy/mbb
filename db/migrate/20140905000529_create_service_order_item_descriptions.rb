class CreateServiceOrderItemDescriptions < ActiveRecord::Migration
  def change
    create_table :service_order_item_descriptions do |t|
      t.string :description , :null => false

      t.timestamps
    end
  end
end
