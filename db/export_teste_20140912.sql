CREATE DATABASE  IF NOT EXISTS `mecanica_development` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `mecanica_development`;
-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (i686)
--
-- Host: 127.0.0.1    Database: mecanica_development
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `service_orders`
--

DROP TABLE IF EXISTS `service_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `nf_created` tinyint(1) DEFAULT '0',
  `order_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `help` tinyint(1) NOT NULL,
  `payment_type` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_service_orders_on_customer_id` (`customer_id`),
  KEY `index_service_orders_on_vehicle_id` (`vehicle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_orders`
--

LOCK TABLES `service_orders` WRITE;
/*!40000 ALTER TABLE `service_orders` DISABLE KEYS */;
INSERT INTO `service_orders` VALUES (1,1,1,'2014-10-24',1,NULL,'2014-09-06 18:40:18','2014-09-11 10:24:29',3,0,1),(2,9,2,NULL,0,NULL,'2014-09-12 00:03:40','2014-09-12 00:03:40',3,0,1),(3,9,2,'2013-01-01',0,'0000-00-00','2014-09-12 00:03:40','2014-09-12 00:03:40',3,0,1),(4,1,1,'2013-10-01',0,'0000-00-00','2014-09-12 00:03:40','2014-09-12 00:03:40',3,0,1);
/*!40000 ALTER TABLE `service_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `neighborhood` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `cep` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cpf_cnpj` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_customers_on_city_id` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Cássio Alexandre de Sousa','Rua Quintino','26','',NULL,'15210000','36084704840','','2014-08-24 17:39:39','2014-09-07 21:49:56','1','',3),(3,'Braslátex Indústria e Comércio de Borracha LTDA','','','',8697,'15140000','59369041000100','osmar@braslatex.com.br','2014-08-24 22:15:05','2014-08-24 22:15:05','1',NULL,NULL),(4,'JDD Distribuidora de Carnes e Derivados LTDA','Av. Nossa Senhora dos Navegantes','663','',8849,'09972260','05279425000103','transjdd@bol.com.br','2014-08-24 22:20:28','2014-08-24 22:20:28','1',NULL,NULL),(5,'Sebo Jales Indústria de Produtos Animais LTDA','Chácara São João','','',8850,'15715000','60995891000195','','2014-08-24 22:42:02','2014-08-24 22:42:02','1','',NULL),(6,'Romabor Comércio Beneficiamento de Borracha e Látex LTDA','Rua Gastão Vidigal','2150','',9413,'15020090','00186002000117','osmar@braslatex.com.br','2014-08-24 22:48:17','2014-08-24 22:48:17','1','Sala 06 Centro Empresarial Odilon Garcez',NULL),(7,'Prefeitura Municipal de Bálsamo','Rua Rio de Janeiro','695','Centro',9413,'15140000','45142353000164','','2014-08-24 22:51:22','2014-08-24 22:51:22','1','',NULL),(8,'Laide Cavalin Marson e Outros','Fazenda Esteio','','',9133,'15150000','08088307000440','','2014-08-24 22:54:14','2014-08-24 22:54:14','1','',NULL),(9,'Aluízio Cavalin','Rua Delegado Pinto de Toledo','3130','Centro',9413,'15010080','35234962849','','2014-08-24 23:57:05','2014-09-11 23:59:22','1','Apto 61 6°Andar',3),(10,'Colina Entalhes e Móveis LTDA ME','Rodovia José Jeronimo de Paula','137','Distrito Industrial',8697,'','62049432000144','meire.colina@hotmail.com','2014-08-25 01:22:39','2014-08-25 01:22:39','1','Caixa Postal 10',NULL),(11,'Borrachas SK LTDA','Rua Julio Mariani','178','Distrito Industrial',8697,'15140000','10779822000176','','2014-08-25 01:28:45','2014-08-25 01:28:45','1','Tanabi S/N - KM 2,2',NULL),(12,'Estofados Rivail Empresarial Eirelli - EPP','Estrada municipal José Domingues Neto','21','Loteamento Santa Inês',9413,'15087000','01062709000184','','2014-08-25 23:12:30','2014-08-25 23:12:30','1','',NULL),(13,'Antônio Lucas da Silva','Rua Edmundo Borduqui','82','',8697,'','08475629806','','2014-08-25 23:14:14','2014-08-25 23:14:14','1','',NULL),(14,'Crippa Maquinas e Equipamentos Eirel','Avenida do Progresso','135','',8697,'15140000','01630356000171','','2014-08-25 23:16:23','2014-08-25 23:16:23','1','',NULL),(15,'Du Bor Representação de Borracha e Transportes','Rua Vereador João Vasques Ebanhas','76','',8697,'15140000','15115548000154','','2014-08-25 23:19:07','2014-08-25 23:19:07','1','',NULL),(16,'Emanuel Henrique de Oliveira','Rua Diolindo Vezzi','75','',8697,'15140000','36758067823','','2014-08-25 23:21:26','2014-08-25 23:21:26','1','',NULL),(17,'Ernesto Santana Moreno','Rua Julio Soares','266','',8697,'15140000','97434183853','','2014-08-25 23:23:59','2014-08-25 23:23:59','1','',NULL),(18,'Fabiano da Silva Carta','Rua Julio Mariani','135','',8697,'15140000','25743396817','','2014-08-25 23:26:23','2014-08-25 23:26:23','1','',NULL),(19,'Ivan Perpetuo da Silva ME','Rua João Radi','833','',8697,'15140000','07739407000184','contato@ivansilva.com.br','2014-08-25 23:28:48','2014-08-25 23:28:48','1','',NULL),(20,'Jairo de Souza Bereta','Rua Rio Grande do Sul','','',8697,'15140000','25081910864','','2014-08-25 23:30:30','2014-08-25 23:30:40','1','',NULL),(21,'José Candido Ângelo','Rua Sergipe','406','',8697,'15140000','02566035881','','2014-08-25 23:32:21','2014-08-25 23:32:21','1','',NULL),(22,'Max-Foam Embalagens IND LTDA','Rua João Fernandes Alves','160','',8697,'15140000','09561720000109','financeiro.maxfoam@hotmail.com','2014-08-25 23:36:10','2014-08-25 23:36:10','1','',NULL),(23,'João Carlos Marangoni','Rodovia José Jerônimo de Paula','70','',8697,'15140000','78489377804','','2014-08-25 23:38:01','2014-08-25 23:38:01','1','',NULL),(24,'João Ibanhos Vasques','Rua Rio de Janeiro','21','Centro',8697,'15140000','01853904805','','2014-08-25 23:39:43','2014-08-25 23:39:43','1','',NULL),(25,'Nelson Conti','Rua Lourenço Diogo Ayala','562','',8697,'15140000','73485942804','','2014-08-25 23:41:55','2014-08-25 23:41:55','1','',NULL),(26,'Luiz  Gonçalves de Aguiar','Rua Candido José de Paula','95','Centro',8697,'15140000','92788335849','','2014-08-25 23:45:29','2014-08-25 23:45:47','1','',NULL),(27,'Fernando Filho Transportadora LTDA ME','Rua Minas Gerais','694','Centro',8697,'15140000','15641279000160','fernandofilhotransportes@hotmail.com','2014-08-25 23:48:27','2014-08-25 23:48:27','1','',NULL);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phones`
--

DROP TABLE IF EXISTS `phones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tp` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_phones_on_customer_id` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phones`
--

LOCK TABLES `phones` WRITE;
/*!40000 ALTER TABLE `phones` DISABLE KEYS */;
INSERT INTO `phones` VALUES (1,'17988261030','CELULAR','Cassio','VIVO',1,'2014-08-24 17:39:39','2014-08-24 17:39:39'),(3,'1732641404','FIXO','','',3,'2014-08-24 22:15:05','2014-08-24 22:15:05'),(4,'1128397038','','Dárcio','',4,'2014-08-24 22:20:28','2014-08-24 22:20:28'),(5,'1140492833','','Júnior','',4,'2014-08-24 22:20:28','2014-08-24 22:20:28'),(6,'17997380829','CELULAR','Poiati','',5,'2014-08-24 22:42:02','2014-08-24 22:42:02'),(7,'1732641404','FIXO','','',6,'2014-08-24 22:48:17','2014-08-24 22:48:17'),(8,'1732649014','FIXO','','',7,'2014-08-24 22:51:22','2014-08-24 22:51:22'),(9,'17997715534','','','',8,'2014-08-24 22:54:14','2014-08-24 22:54:14'),(10,'17997715587','CELULAR','','',9,'2014-08-25 00:20:21','2014-08-25 00:20:21'),(11,'1732328357','FIXO','','',9,'2014-08-25 00:20:21','2014-08-25 00:20:21'),(12,'1732641143','FIXO','','',10,'2014-08-25 01:22:39','2014-08-25 01:22:39'),(13,'1732641470','FIXO','Tel Balsámo','',11,'2014-08-25 01:29:20','2014-08-25 01:29:20'),(14,'17997677887','CELULAR','Valdair','',12,'2014-08-25 23:12:30','2014-08-25 23:12:30'),(15,'17996134383','CELULAR','Lucas','',12,'2014-08-25 23:12:30','2014-08-25 23:12:30'),(16,'17981255843','CELULAR','Rivail','',12,'2014-08-25 23:12:30','2014-08-25 23:12:30'),(17,'1732274894','FIXO','','',12,'2014-08-25 23:12:30','2014-08-25 23:12:30'),(18,'1732647370','FIXO','','',13,'2014-08-25 23:14:14','2014-08-25 23:14:14'),(19,'1732649100','FIXO','','',14,'2014-08-25 23:16:23','2014-08-25 23:16:23'),(20,'1732641033','FIXO','','',15,'2014-08-25 23:19:07','2014-08-25 23:19:07'),(21,'17996165513','CELULAR','Dedê','',16,'2014-08-25 23:21:26','2014-08-25 23:21:26'),(22,'17991596410','CELULAR','','',16,'2014-08-25 23:21:26','2014-08-25 23:21:26'),(23,'17981326899','CELULAR','','',17,'2014-08-25 23:23:59','2014-08-25 23:23:59'),(24,'1732647563','FIXO','','',17,'2014-08-25 23:23:59','2014-08-25 23:23:59'),(25,'17997359807','CELULAR','','',18,'2014-08-25 23:26:23','2014-08-25 23:26:23'),(26,'1732641256','FIXO','','',18,'2014-08-25 23:26:23','2014-08-25 23:26:23'),(27,'17996324343','CELULAR','Reginaldo','',19,'2014-08-25 23:28:48','2014-08-25 23:28:48'),(28,'1732641593','FIXO','','',19,'2014-08-25 23:28:48','2014-08-25 23:28:48'),(29,'17997919673','CELULAR','','',20,'2014-08-25 23:30:30','2014-08-25 23:30:30'),(30,'17981624761','CELULAR','','',21,'2014-08-25 23:32:21','2014-08-25 23:32:21'),(31,'17997769607','CELULAR','Marquinho','',22,'2014-08-25 23:36:10','2014-08-25 23:36:10'),(32,'1732641087','FIXO','','',22,'2014-08-25 23:36:10','2014-08-25 23:36:10'),(33,'1732641429','FIXO','','',23,'2014-08-25 23:38:01','2014-08-25 23:38:01'),(34,'17996110920','CELULAR','','',24,'2014-08-25 23:39:43','2014-08-25 23:39:43'),(35,'17997654143','CELULAR','','',25,'2014-08-25 23:41:55','2014-08-25 23:41:55'),(36,'1732641436','FIXO','','',25,'2014-08-25 23:41:55','2014-08-25 23:41:55'),(37,'17996243090','CELULAR','','',26,'2014-08-25 23:45:29','2014-08-25 23:45:29'),(38,'17981317070','CELULAR','','',27,'2014-08-25 23:48:27','2014-08-25 23:48:27'),(39,'1732641315','FIXO','','',27,'2014-08-25 23:48:27','2014-08-25 23:48:27'),(40,'11111111111','CELULAR','','',1,'2014-09-07 22:06:55','2014-09-07 22:06:55');
/*!40000 ALTER TABLE `phones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state_registrations`
--

DROP TABLE IF EXISTS `state_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state_registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `description` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_state_registrations_on_customer_id` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state_registrations`
--

LOCK TABLES `state_registrations` WRITE;
/*!40000 ALTER TABLE `state_registrations` DISABLE KEYS */;
INSERT INTO `state_registrations` VALUES (1,'197003650112',3,'2014-08-24 22:15:05','2014-08-24 22:15:05',NULL,NULL),(2,'286244545114',4,'2014-08-24 22:20:28','2014-08-24 22:20:28',NULL,NULL),(3,'722000180118',5,'2014-08-24 22:42:02','2014-08-24 22:42:02',NULL,NULL),(4,'647003224116',6,'2014-08-24 22:48:17','2014-08-24 22:48:17',NULL,NULL),(5,'462071786119',8,'2014-08-24 22:54:14','2014-08-24 22:54:14',NULL,NULL),(6,'462071777118',9,'2014-08-24 23:57:05','2014-08-25 00:22:01','Fazenda Nova Santa Rosa',5445),(7,'285029070',9,'2014-08-25 01:16:50','2014-08-25 01:16:50','Fazenda Nova Esp. do Sul',4054),(8,'285028928',9,'2014-08-25 01:16:50','2014-08-25 01:16:50','Fazenda Cavalin',4054),(9,'605059374113',9,'2014-08-25 01:16:50','2014-08-25 01:16:50','Estância Paraíso',9347),(10,'444054953114',9,'2014-08-25 01:16:50','2014-08-25 01:16:50','Fazenda Nova Santa Helena',9112),(11,'277061405117',9,'2014-08-25 01:16:50','2014-08-25 01:16:50','Fazenda São Luiz',8835),(12,'197002370119',10,'2014-08-25 01:22:39','2014-08-25 01:22:39','',NULL),(13,'681091007112',11,'2014-08-25 01:28:45','2014-08-25 01:28:45','',9469),(14,'647265335112',12,'2014-08-25 23:12:30','2014-08-25 23:12:30','',NULL),(15,'197008378116',22,'2014-08-25 23:36:10','2014-08-25 23:36:10','',NULL);
/*!40000 ALTER TABLE `state_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `social_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fantasy_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_neighborhood` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnpj` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `im` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observation` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_vehicles`
--

DROP TABLE IF EXISTS `customer_vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_customer_vehicles_on_customer_id` (`customer_id`),
  KEY `index_customer_vehicles_on_vehicle_id` (`vehicle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_vehicles`
--

LOCK TABLES `customer_vehicles` WRITE;
/*!40000 ALTER TABLE `customer_vehicles` DISABLE KEYS */;
INSERT INTO `customer_vehicles` VALUES (1,1,1,'2014-09-07 22:15:33','2014-09-07 22:15:33'),(2,9,2,'2014-09-11 23:59:22','2014-09-11 23:59:22');
/*!40000 ALTER TABLE `customer_vehicles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_order_items`
--

DROP TABLE IF EXISTS `service_order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `soi_date` date DEFAULT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `service_order_item_description_id` int(11) DEFAULT NULL,
  `service_order_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_order_items`
--

LOCK TABLES `service_order_items` WRITE;
/*!40000 ALTER TABLE `service_order_items` DISABLE KEYS */;
INSERT INTO `service_order_items` VALUES (3,'2014-09-23',300.00,26,1,'2014-09-07 23:11:22','2014-09-09 00:39:20'),(5,'2014-10-01',400.00,25,1,'2014-09-09 00:37:00','2014-09-09 00:37:00'),(6,'2014-09-09',50.00,27,1,'2014-09-11 10:24:29','2014-09-11 10:24:29'),(7,'2014-09-10',0.40,28,1,'2014-09-11 23:41:15','2014-09-11 23:41:15'),(8,'2014-09-10',3000.00,29,2,'2014-09-12 00:03:40','2014-09-12 00:03:40'),(9,'2014-09-10',3000.00,29,3,'2014-09-12 00:03:40','2014-09-12 00:03:40'),(10,'2014-09-10',500.00,29,4,'2014-09-12 00:03:40','2014-09-12 00:03:40');
/*!40000 ALTER TABLE `service_order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_order_item_descriptions`
--

DROP TABLE IF EXISTS `service_order_item_descriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_order_item_descriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_order_item_descriptions`
--

LOCK TABLES `service_order_item_descriptions` WRITE;
/*!40000 ALTER TABLE `service_order_item_descriptions` DISABLE KEYS */;
INSERT INTO `service_order_item_descriptions` VALUES (1,'teset','2014-09-06 20:13:13','2014-09-06 20:13:13'),(2,'teste','2014-09-06 20:19:17','2014-09-06 20:19:17'),(3,'teste','2014-09-06 20:23:04','2014-09-06 20:23:04'),(4,'teste','2014-09-06 20:27:20','2014-09-06 20:27:20'),(5,'teste','2014-09-06 20:33:32','2014-09-06 20:33:32'),(6,'teste','2014-09-06 20:42:31','2014-09-06 20:42:31'),(7,'teste','2014-09-06 20:44:32','2014-09-06 20:44:32'),(8,'teste','2014-09-06 20:48:38','2014-09-06 20:48:38'),(9,'teste','2014-09-06 20:51:40','2014-09-06 20:51:40'),(10,'teste','2014-09-06 20:58:17','2014-09-06 20:58:17'),(11,'teste','2014-09-06 21:00:39','2014-09-06 21:00:39'),(12,'teste','2014-09-06 21:03:42','2014-09-06 21:03:42'),(13,'teste','2014-09-06 21:04:40','2014-09-06 21:04:40'),(14,'teste','2014-09-06 21:05:04','2014-09-06 21:05:04'),(15,'teste','2014-09-06 21:05:29','2014-09-06 21:05:29'),(16,'teste','2014-09-06 21:06:41','2014-09-06 21:06:41'),(17,'teste','2014-09-06 21:07:30','2014-09-06 21:07:30'),(18,'teste','2014-09-06 21:08:06','2014-09-06 21:08:06'),(19,'teste','2014-09-06 21:11:03','2014-09-06 21:11:03'),(20,'teste','2014-09-06 21:17:10','2014-09-06 21:17:10'),(21,'teste','2014-09-06 21:17:58','2014-09-06 21:17:58'),(22,'teste','2014-09-06 21:18:26','2014-09-06 21:18:26'),(23,'teste','2014-09-06 21:20:33','2014-09-06 21:20:33'),(24,'teste','2014-09-06 21:21:47','2014-09-06 21:21:47'),(25,'cassio','2014-09-06 22:12:00','2014-09-06 22:12:00'),(26,'01 litro de oléo para motor diesel','2014-09-07 23:10:56','2014-09-07 23:10:56'),(27,'01 filtro de ar','2014-09-11 10:24:13','2014-09-11 10:24:13'),(28,'02 parafusos cabeça chata','2014-09-11 23:40:50','2014-09-11 23:40:50'),(29,'30 litros óleo de motor','2014-09-12 00:00:03','2014-09-12 00:00:03');
/*!40000 ALTER TABLE `service_order_item_descriptions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-09-12 23:31:37
