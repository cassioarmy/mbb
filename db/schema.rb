# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150615150354) do

  create_table "cities", force: true do |t|
    t.string   "name"
    t.integer  "state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "companies", force: true do |t|
    t.string   "social_name"
    t.string   "fantasy_name"
    t.string   "address"
    t.string   "address_number"
    t.string   "address_neighborhood"
    t.string   "email"
    t.string   "cnpj"
    t.string   "ie"
    t.string   "im"
    t.text     "observation"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "phone"
    t.string   "phone2"
  end

  create_table "customer_vehicles", force: true do |t|
    t.integer  "customer_id"
    t.integer  "vehicle_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "customer_vehicles", ["customer_id"], name: "index_customer_vehicles_on_customer_id", using: :btree
  add_index "customer_vehicles", ["vehicle_id"], name: "index_customer_vehicles_on_vehicle_id", using: :btree

  create_table "customers", force: true do |t|
    t.string   "name",         limit: 100
    t.string   "address",      limit: 80
    t.string   "number",       limit: 4
    t.string   "neighborhood", limit: 50
    t.integer  "city_id"
    t.string   "cep",          limit: 8
    t.string   "cpf_cnpj",     limit: 14
    t.string   "email",        limit: 100
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status",       limit: 1,   default: "1"
    t.string   "address2"
    t.integer  "user_id"
  end

  add_index "customers", ["city_id"], name: "index_customers_on_city_id", using: :btree

  create_table "email_logs", force: true do |t|
    t.string   "email_from"
    t.integer  "service_order_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "email_logs", ["service_order_id"], name: "index_email_logs_on_service_order_id", using: :btree

  create_table "phones", force: true do |t|
    t.string   "number",      limit: 11
    t.string   "tp",          limit: 10
    t.string   "contact",     limit: 50
    t.string   "provider",    limit: 10
    t.integer  "customer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "phones", ["customer_id"], name: "index_phones_on_customer_id", using: :btree

  create_table "roles", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles_users", force: true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "roles_users", ["role_id"], name: "index_roles_users_on_role_id", using: :btree
  add_index "roles_users", ["user_id"], name: "index_roles_users_on_user_id", using: :btree

  create_table "service_order_item_descriptions", force: true do |t|
    t.string   "description", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "service_order_items", force: true do |t|
    t.date     "soi_date"
    t.decimal  "unit_price",                        precision: 10, scale: 2, null: false
    t.integer  "service_order_item_description_id",                          null: false
    t.integer  "service_order_id",                                           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "service_orders", force: true do |t|
    t.integer  "customer_id",                                                        null: false
    t.integer  "vehicle_id"
    t.date     "payment_date"
    t.boolean  "nf_created",                                         default: false
    t.date     "order_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.boolean  "help",                                                               null: false
    t.integer  "payment_type", limit: 1
    t.boolean  "paid",                                               default: false
    t.string   "observations", limit: 4000
    t.decimal  "discount",                  precision: 10, scale: 2, default: 0.0,   null: false
  end

  add_index "service_orders", ["customer_id"], name: "index_service_orders_on_customer_id", using: :btree
  add_index "service_orders", ["vehicle_id"], name: "index_service_orders_on_vehicle_id", using: :btree

  create_table "state_registrations", force: true do |t|
    t.string   "number",      limit: 50
    t.integer  "customer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description", limit: 250
    t.integer  "city_id"
  end

  add_index "state_registrations", ["customer_id"], name: "index_state_registrations_on_customer_id", using: :btree

  create_table "states", force: true do |t|
    t.string   "name",       limit: 50
    t.string   "uf",         limit: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "full_name"
    t.string   "phone"
    t.string   "cel_phone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "vehicles", force: true do |t|
    t.string   "description",   limit: 100
    t.integer  "city_id"
    t.string   "license_plate", limit: 7
    t.string   "year_model",    limit: 8
    t.string   "chassi",        limit: 17
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status",        limit: 1,   default: "1"
    t.integer  "user_id"
  end

end
